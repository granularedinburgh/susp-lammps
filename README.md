# My project's README

This project has modifications to several [LAMMPS](http://lammps.sandia.gov) source files that can be built with LAMMPS version Nov, 2016 found on [github](http://github.com/lammps/).

There are also some post-processing tools written using cpp to analyse LAMMPS dump files.
