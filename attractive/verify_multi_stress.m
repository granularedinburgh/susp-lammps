%-------------------------------------------------------------------------%
% Script to verify stress contributions due to multiple particles
% Date Sep 1, 2017
% Modified by Ranga for particles interacting with repulsive forces.
%-------------------------------------------------------------------------%

clear;
%close all;

display('Reading parameters');
%muf = nuf*rhof;


Kn=10000;

folder = '/tmp/ranga/two_particles/adhesive/';
filename2 = strcat(folder,'dump.lammpstrj');

gdot = 100.0; % 0.05;

N = 51; %Number of steps

nPart = 2;


dt=1e-5;



%% Read in s.p

display('Reading particle output file');

f1 = 'timestep'; v1 = zeros(1,1);
f2 = 'natoms'; v2 = zeros(1,1);
f3 = 'xbox'; v3 = zeros(1,3);
f4 = 'ybox'; v4 = zeros(1,3);
f5 = 'zbox'; v5 = zeros(1,3);
f6 = 'values'; v6 = zeros(1,19);


pdata(1:N,1:nPart) = struct(f1, v1, f2, v2, f3, v3, f4, v4, f5, v5, f6, v6);


file = fopen(filename2);

for i=1:N
    pdata(i,1).timestep = fscanf(file, '%*s %*s\n%d\n', 1);
    pdata(i,1).natoms = fscanf(file, '%*s %*s %*s %*s\n%d\n', 1);
    pdata(i,1).xbox = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f\n', [1, 3]);
    pdata(i,1).ybox = fscanf(file, '%f %f %f\n', [1, 3]);
    pdata(i,1).zbox = fscanf(file, '%f %f %f\n', [1, 3]);
    
    for j=1:pdata(i,1).natoms
        if j == 1
            %dump id2 all custom 100 s.p id type radius mass x y z vx vy vz fx fy fz tqx tqy tqz omegax omegay omegaz
            tmp = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        else
            tmp = fscanf(file, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        end
        pdata(i,tmp(1)).values = tmp;
        
    end
    
end
fclose(file);


%% Some Variable declaration
display('Declaring variables');
fytot=zeros([N nPart]);
fylub=zeros([N nPart]);
fyfric=zeros([N nPart]);

xpos=zeros([N nPart]);
ypos=zeros([N nPart]);
zpos=zeros([N nPart]);


Fx=zeros([N nPart]);
Fy=zeros([N nPart]);
Fz=zeros([N nPart]);
type=zeros([N nPart]);
time=zeros([N 1]);
f_dimensionless = zeros(N,1);
flubanalyticaln = zeros(N,1);



for i = 1:N %size(s,2)
    for j=1:pdata(i,1).natoms
        xpos(i,j)=pdata(i,j).values(5);
        ypos(i,j)=pdata(i,j).values(6);
        zpos(i,j)=pdata(i,j).values(7);
        
        type(i,j)=pdata(i,j).values(2);
        
          
        Fx(i,j)=pdata(i,j).values(11);
        Fy(i,j)=pdata(i,j).values(12);
        Fz(i,j)=pdata(i,j).values(13);
    end
    time(i)=dt*pdata(i,1).timestep;
end



lx = pdata(1,1).xbox(2)- pdata(1,1).xbox(1);
ly = pdata(1,1).ybox(2)- pdata(1,1).ybox(1);
lz = pdata(1,1).zbox(2)- pdata(1,1).zbox(1);
box_vol=lx*ly*lz;

initial=dt*0;
delx = (time-initial)*gdot*ly; % assume shear rate = dvx/dy box displacement
%delx = disp; % -L/2 <delx < L/2
delv = gdot*ly;

Scontxy=zeros(N,1);
Slubxy=zeros(N,1);

%% Cut off and force parameters
cut_inner=[1.0 1.2; 1.2 1.4];
cut=[1.05 1.25; 1.25 1.45];
cutsq=cut.^2;
force_inner=[1000 1000; 1000 1000];
force_outer=[0 0; 0 0];

%% Computation of force for comparison
display('Computing forces')

for i = 1:N %size(s,2)
    f_dimensionless(i) =sqrt(Fx(i,1).^2+Fy(i,1).^2+Fz(i,1).^2);
    %f_dimensionless(i) =Fy(i,1);

    
    for j=1:pdata(i,1).natoms
        r1=pdata(i,j).values(3);
        jtype=type(i,j);
        
        for k=j+1:pdata(i,1).natoms
            r2=pdata(i,k).values(3);
            ktype=type(i,k);
            %calculate contact forces
            
            [ccdx21,ccdy21,ccdz21,ccd21]=minimage(lx,ly,lz,delx(i),xpos(i,j),ypos(i,j),zpos(i,j),xpos(i,k),ypos(i,k),zpos(i,k));
            
            nx=ccdx21/ccd21;
            ny=ccdy21/ccd21;
            nz=ccdz21/ccd21;
            eps21 = ccd21 - r1 - r2;
            if eps21<0
                fx1=Kn*eps21*nx;
                fy1=Kn*eps21*ny;
                fz1=Kn*eps21*nz;
            else
                fx1=0;
                fy1=0;
                fz1=0;
            end
            
            %Sum stress contribution due to contact forces
            Scontxy(i)=Scontxy(i)+fx1*ccdy21;
            
    
 
        cons=(force_outer(jtype,ktype) - force_inner(jtype,ktype))/(cut(jtype,ktype) - cut_inner(jtype,ktype));
     
      if (ccd21 < cut(jtype,ktype) && ccd21>0.0) 
        if (ccd21 < cut_inner(jtype,ktype)) 
	    %constant force
          fpair = force_inner(jtype,ktype)/ccd21;

        else 
        
	%Linear force
          fpair = force_inner(jtype,ktype)/ccd21 + ( cons * (1.0 - cut_inner(jtype,ktype)/ccd21) );
        end
	
        fx2 = -ccd21*nx*fpair;
        fy2 = -ccd21*ny*fpair;
        fz2 = -ccd21*nz*fpair;

      else
        fx2 = 0;
        fy2 = 0;
        fz2 = 0;
       
      end
            
                
            flubanalyticaln(i)=sqrt((fx1+fx2).^2+(fy1+fy2).^2+(fz1+fz2).^2);
            %flubanalyticaln(i)=sqrt((0+fx2).^2+(0+fy2).^2+(0+fz2).^2);

            Slubxy(i)=Slubxy(i)+fy2*ccdx21;
            
        end
        
    end
    
end


for i = 1:N %size(s,2)
    
    Scontxy(i)=Scontxy(i)/box_vol;
    Slubxy(i)=Slubxy(i)/box_vol;
end



%% Plotting of solution

display('Plot of analytical solution for stress')
% 
% 
%    subplot(2,1,1);
%    plot(time, f_dimensionless, 'o','LineWidth', 2);
%     hold on;
%     plot(time, flubanalyticaln, '-r','linewidth', 2);
%     
%     %xlabel('Time');
%     ylabel('Force');
%     set(gca,'XScale','log');
    %hold off;
    
    

    
    
    
    %plot(time,-SC_t, '-k','LineWidth', 2);
    
    
    %plot(time,-SC_con, '-r','LineWidth', 2);
    


sdata=dlmread(strcat(folder,'stressOutputlub'), ' ', 1, 0);
plot(time(2:end),Slubxy(2:end), '-b','LineWidth', 2);
hold all;
plot(time(2:end),Scontxy(2:end), '-r','LineWidth', 2);
plot(time(2:end),Slubxy(2:end)+Scontxy(2:end), '-k','LineWidth', 2);
% 
plot(sdata(:,1)*dt,-sdata(:,2), 'ok','LineWidth', 2);
% 
plot(sdata(:,1)*dt,-sdata(:,3), 'or','LineWidth', 2);
plot(sdata(:,1)*dt,-sdata(:,4), 'ob','LineWidth', 2);
% 
% 
set(gca,'XScale','log');
xlabel('Time');
ylabel('Stress');
hold off;
