/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Julien Sindt (UoE - 20170216)
   Updated and Simplified by Ranga Radhakrishnan (28/08)
   Pair style is called with:
   pair_style attractive $F(rmin) $F(rmax) $rmin $rmax $flag_F(r<rmin)=F(rmin)
   ------------------------------------------------------------------------- */

#include <math.h>
#include <stdlib.h>
#include "pair_ATTRACTIVE.h"
#include "atom.h"
#include "comm.h"
#include "force.h"
#include "neigh_list.h"
#include "memory.h"
#include "error.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

PairAttractive::PairAttractive(LAMMPS *lmp) : Pair(lmp)
{
  writedata = 1;
}

/* ---------------------------------------------------------------------- */

PairAttractive::~PairAttractive()
{
  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(cutsq);

    memory->destroy(cut);
    memory->destroy(cut_inner);
    memory->destroy(force_inner);
    memory->destroy(force_outer);
  }
}

/* ---------------------------------------------------------------------- */

void PairAttractive::compute(int eflag, int vflag)
{
  int i,j,ii,jj,inum,jnum,itype,jtype;
  double xtmp,ytmp,ztmp,delx,dely,delz;
  double r,rsq,eattrct,fpair;
  int *ilist,*jlist,*numneigh,**firstneigh;

  eattrct = 0.0;
  if (eflag || vflag) ev_setup(eflag,vflag);
  else evflag = vflag_fdotr = 0;

  double **x = atom->x;
  double **f = atom->f;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  int newton_pair = force->newton_pair;

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    itype = type[i];
    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      j &= NEIGHMASK;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;
      r = sqrt(rsq);
      jtype = type[j];
      double cons=(force_outer[itype][jtype] - force_inner[itype][jtype])/(cut[itype][jtype] - cut_inner[itype][jtype]);
     
      if (r < cut[itype][jtype] && r>0.0) 
      {

	double old_t=0.0;
        if (r < cut_inner[itype][jtype]) 
        {
	//constant force
          fpair = force_inner[itype][jtype]/r;
	//Potential energy due to the constant force summed with a constant due to the second part of the force
          if(eflag)
	  {
	 old_t = (cut_inner[itype][jtype]-cut[itype][jtype])*force_inner[itype][jtype]-cons*( (cutsq[itype][jtype]-pow(cut_inner[itype][jtype],2))/2.0-(cut[itype][jtype]-cut_inner[itype][jtype])*cut_inner[itype][jtype]);
		  eattrct =-old_t +force_inner[itype][jtype]*(cut_inner[itype][jtype]-r); 
	  }
        } 
        else 
        {
	//Linear force
          fpair = force_inner[itype][jtype]/r + ( cons * (1.0 - cut_inner[itype][jtype]/r) );
	//Potential energy due to the linear force
         if(eflag) eattrct = -( (r-cut[itype][jtype])*force_inner[itype][jtype]-cons*( (cutsq[itype][jtype]-rsq)/2.0-(cut[itype][jtype]-r)*cut_inner[itype][jtype]) );
        }
        f[i][0] += delx*fpair;
        f[i][1] += dely*fpair;
        f[i][2] += delz*fpair;
        if (newton_pair || j < nlocal) {
          f[j][0] -= delx*fpair;
          f[j][1] -= dely*fpair;
          f[j][2] -= delz*fpair;
        }

    //printf("i:%d,j:%d,r:%g, inner:%g, outer:%g, fin:%g, fout:%g, fpair:%g\n",itype,jtype,r,cut_inner[itype][jtype],cut[itype][jtype],force_inner[itype][jtype],force_outer[itype][jtype],fpair*r);
        if (evflag) ev_tally(i,j,nlocal,newton_pair,eattrct,0.0,fpair,delx,dely,delz);
      }
    }
 }

  if (vflag_fdotr) virial_fdotr_compute();
}

/* ----------------------------------------------------------------------
   allocate all arrays
------------------------------------------------------------------------- */

void PairAttractive::allocate()
{
  allocated = 1;
  int n = atom->ntypes;

  memory->create(setflag,n+1,n+1,"pair:setflag");
  for (int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++)
      setflag[i][j] = 0;

  memory->create(cutsq,n+1,n+1,"pair:cutsq");

  memory->create(cut,n+1,n+1,"pair:cut");
  memory->create(cut_inner,n+1,n+1,"pair:cut_inner");
  memory->create(force_inner,n+1,n+1,"pair:force_inner");
  memory->create(force_outer,n+1,n+1,"pair:force_outer");
}

/* ----------------------------------------------------------------------
   global settings
------------------------------------------------------------------------- */

void PairAttractive::settings(int narg, char **arg)
{
  if (narg != 4) error->all(FLERR,"Illegal pair_style command");

  force_inner_global = force->numeric(FLERR,arg[0]);
  force_outer_global = force->numeric(FLERR,arg[1]);
  cut_inner_global = force->numeric(FLERR,arg[2]);
  cut_global = force->numeric(FLERR,arg[3]);

  if (cut_inner_global <= 0.0 || cut_inner_global > cut_global)
    error->all(FLERR,"Illegal pair_style command");

  // reset cutoffs that have been explicitly set

  if (allocated) {
    int i,j;
    for (i = 1; i <= atom->ntypes; i++)
      for (j = i+1; j <= atom->ntypes; j++)
        if (setflag[i][j]) {
          force_inner[i][j] = force_inner_global;
          force_outer[i][j] = force_outer_global;
          cut_inner[i][j] = cut_inner_global;
          cut[i][j] = cut_global;
        }
  }
}

/* ----------------------------------------------------------------------
   set coeffs for one or more type pairs
------------------------------------------------------------------------- */

void PairAttractive::coeff(int narg, char **arg)
{
  if (narg != 6)
    error->all(FLERR,"Incorrect args for pair coefficients");
  if (!allocated) allocate();

  int ilo,ihi,jlo,jhi;
  force->bounds(FLERR,arg[0],atom->ntypes,ilo,ihi);
  force->bounds(FLERR,arg[1],atom->ntypes,jlo,jhi);

  double force_inner_one = force->numeric(FLERR,arg[2]);
  double force_outer_one = force->numeric(FLERR,arg[3]);

  double cut_inner_one = cut_inner_global;
  double cut_one = cut_global;
  cut_inner_one = force->numeric(FLERR,arg[4]);
  cut_one = force->numeric(FLERR,arg[5]);

  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    for (int j = MAX(jlo,i); j <= jhi; j++) {
      force_inner[i][j] = force_inner_one;
      force_outer[i][j] = force_outer_one;
      cut_inner[i][j] = cut_inner_one;
      cut[i][j] = cut_one;
      setflag[i][j] = 1;
      count++;
    }
  }

  if (count == 0) error->all(FLERR,"Incorrect args for pair coefficients");
}

/* ----------------------------------------------------------------------
   init for one type pair i,j and corresponding j,i
------------------------------------------------------------------------- */

double PairAttractive::init_one(int i, int j)
{
  if (setflag[i][j] == 0) {
    force_inner[i][j] = mix_distance(force_inner[i][i],force_inner[j][j]);
    force_outer[i][j] = mix_distance(force_outer[i][i],force_outer[j][j]);
    cut_inner[i][j] = mix_distance(cut_inner[i][i],cut_inner[j][j]);
    cut[i][j] = mix_distance(cut[i][i],cut[j][j]);
  }

  force_inner[j][i] = force_inner[i][j];
  force_outer[j][i] = force_outer[i][j];
  cut_inner[j][i] = cut_inner[i][j];
  cut[j][i] = cut[i][j];

  return cut[i][j];
}

/* ----------------------------------------------------------------------
   proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairAttractive::write_restart(FILE *fp)
{
  write_restart_settings(fp);

  int i,j;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      fwrite(&setflag[i][j],sizeof(int),1,fp);
      if (setflag[i][j]) {
        fwrite(&force_inner[i][j],sizeof(double),1,fp);
        fwrite(&force_outer[i][j],sizeof(double),1,fp);
        fwrite(&cut_inner[i][j],sizeof(double),1,fp);
        fwrite(&cut[i][j],sizeof(double),1,fp);
      }
    }
}

/* ----------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairAttractive::read_restart(FILE *fp)
{
  read_restart_settings(fp);
  allocate();

  int i,j;
  int me = comm->me;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      if (me == 0) fread(&setflag[i][j],sizeof(int),1,fp);
      MPI_Bcast(&setflag[i][j],1,MPI_INT,0,world);
      if (setflag[i][j]) {
        if (me == 0) {
          fread(&force_inner[i][j],sizeof(double),1,fp);
          fread(&force_outer[i][j],sizeof(double),1,fp);
          fread(&cut_inner[i][j],sizeof(double),1,fp);
          fread(&cut[i][j],sizeof(double),1,fp);
        }
        MPI_Bcast(&force_inner[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&force_outer[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&cut_inner[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&cut[i][j],1,MPI_DOUBLE,0,world);
      }
    }
}

/* ----------------------------------------------------------------------
   proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairAttractive::write_restart_settings(FILE *fp)
{
  fwrite(&cut_inner_global,sizeof(double),1,fp);
  fwrite(&cut_global,sizeof(double),1,fp);
  fwrite(&offset_flag,sizeof(int),1,fp);
  fwrite(&mix_flag,sizeof(int),1,fp);
}

/* ----------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairAttractive::read_restart_settings(FILE *fp)
{
  if (comm->me == 0) {
    fread(&cut_inner_global,sizeof(double),1,fp);
    fread(&cut_global,sizeof(double),1,fp);
    fread(&offset_flag,sizeof(int),1,fp);
    fread(&mix_flag,sizeof(int),1,fp);
  }
  MPI_Bcast(&cut_inner_global,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&cut_global,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&offset_flag,1,MPI_INT,0,world);
  MPI_Bcast(&mix_flag,1,MPI_INT,0,world);
}

/* ----------------------------------------------------------------------
   proc 0 writes to data file
------------------------------------------------------------------------- */

void PairAttractive::write_data(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    fprintf(fp,"%d %g %g %g %g\n",i,force_inner[i][i],force_outer[i][i], cut_inner[i][i],cut[i][i]); 
}

/* ----------------------------------------------------------------------
   proc 0 writes all pairs to data file
------------------------------------------------------------------------- */

void PairAttractive::write_data_all(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    for (int j = i; j <= atom->ntypes; j++)
      fprintf(fp,"%d %d %g %g %g %g\n",i,j,force_inner[i][j],force_outer[i][j],cut_inner[i][j],cut[i][j]);
}

/* ---------------------------------------------------------------------- */
double PairAttractive::single(int i, int j, int itype, int jtype, double rsq,
    double factor_coul, double factor_lj, double &fforce)
{
  double phi_attrct=0.0,r;

  double cons=(force_outer[itype][jtype] - force_inner[itype][jtype])/(cut[itype][jtype] - cut_inner[itype][jtype]);
   r = sqrt(rsq);
  if (r < cut[itype][jtype] && r>0.0) 
  {
    if (r < cut_inner[itype][jtype]) 
    {
      fforce = force_inner[itype][jtype]/r;
      double old_t= (cut_inner[itype][jtype]-cut[itype][jtype])*force_inner[itype][jtype]-cons*( (cutsq[itype][jtype]-pow(cut_inner[itype][jtype],2))/2.0-(cut[itype][jtype]-cut_inner[itype][jtype])*cut_inner[itype][jtype]);
      phi_attrct =-old_t +force_inner[itype][jtype]*(cut_inner[itype][jtype]-r); 
    } 
    else
    {
     fforce = force_inner[itype][jtype]/r + ( cons * (1.0 - cut_inner[itype][jtype]/r) );
     phi_attrct = -( (r-cut[itype][jtype])*force_inner[itype][jtype]-cons*( (cutsq[itype][jtype]-rsq)/2.0-(cut[itype][jtype]-r)*cut_inner[itype][jtype]) );
    }
  } 
 // printf("single: %g\n",fforce);
  return phi_attrct;
}
