A linear attractive or repulsive force for mimicking electrostatic repulsive force at very short ranges.

To use this class, please compile this with LAMMPS Nov 17, 2016 version. Since this class was made by modifying pair style soft in LAMMPS, a similar modification to pair_style soft can be made for other versions of LAMMPS.

## How to use this pair style
## Syntax
```pair_style  attractive force_inner force_outer inner_cutoff outer_cutoff```


* force_inner: is the force at the inner cutoff (force units)
* force_outer: is the force at the outer cutoff (force units)
* inner_cutoff: inner cutoff of the force (distance units)
* outer_cutoff: (distance units)

## Examples
```
	pair_style  attractive 10 0 1.05 1.00
	pair_coeff * *
	
	pair_style  attractive -10 0 1.05 1.00
	pair_coeff 1 1 1.05 1.00
	pair_coeff 2 2 1.4 1.45
	pair_coeff 1 2 1.2 1.25
```


## Description
```
F(x) = force_inner, x<inner_cutoff
F(x) = force_inner+(force_inner -force_outer)*x/(outer_cutoff-inner_cutoff), inner_cutoff<= x <outer_cutoff
F(x) = force_outer, x>=outer_cutoff
```


A linear repulsive force with inner and outer cutoff as specified above. To make the force attractive, one can change the sign to be negative as shown in the example above.

