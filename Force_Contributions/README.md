# Force contributions due to contact and lubrication forces
##Explanation
The LAMMPS code in this project are to specifically dump contact forces, and lubrication forces in the normal and tangential directions between any pair of atoms. This can be used to calculate the force directions, distributions, and fraction of slipping contacts.

It is a hack based on the single method (which calculates the forces at the end of a time step when requested by the user), and is therefore not general for any two forces in LAMMPS. Even the contact and lubrication force files have been changed to accommodate the forces in specific format.

The command *compute pair/local* is used to compute the properties using the single method. The **pair overlay/hybridsingle** command has been modified from *pair overlay/hybrid* to add up the contributions from the individual single function. I have added a single function for *lubricate/Simple* to make all the components of the lubrication force output to be zero except the last three components and called it as **lubricate/Simpler**. Similarly, I have modified *pair gran/hooke/history* to make the last three components of **pair gran/hooke/single** to be zero. 

The individual components from the force when a command such as,
'''compute 1 all pair/local p1 p2 p3 p4 p5 p6 p7 p8 p9 p10'''
is used in combination with the dump local command are as follows

1. p1 = contact slip number (0=no contact, -1=no slip contact,1=slip contact)
2. p2 = Magnitude of the tangential force from contact
3. p3 = Magnitude of the normal force from contact
4. p4 = Tangential force in x direction from contact
5. p5 = Tangential force in y direction from contact
6. p6 = Normal force in x direction from contact
7. p7 = Normal force in y direction from contact
8. p8 = Lubrication force in x direction
9. p9 = Lubrication force in y direction
10. p10 = Lubrication force in z direction

Please check collision.lmp file for use case. Note that the single funtion in *pair_CLM.cpp* has has been updated to have the same components as mentioned above.

##Compilation instructions
Copy these files to your local LAMMPS installation and recompile.

##Test Cases
* Verified for two particle simulations that the new simulations give the same results as the one using a combination of *gran/hooke/history + lubricate/simple*. 
* Verified for multiple particle simulations that the new simulations give the same results as the one using a combination of *gran/hooke/history + lubricate/simple*. 

