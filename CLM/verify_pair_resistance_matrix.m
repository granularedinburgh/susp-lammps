%-------------------------------------------------------------------------%
% Script to analyse/verify pair style resistance_matrix_flags 
% in LAMMPS/LIGGGHTS
% Simulation results between two particles are compared to the analytical
% solution (/DEM formula)
% 
%
% Original Author: Tim Najuch, 3/6/2016
% Modified by: Ranga Radhakrishnan 10/4/2017
%-------------------------------------------------------------------------%
clear;
close all;

%% Parameters of case
% 
display('Reading parameters');

rhos = 1;
rhof = 1;
nuf = 1;
%muf = nuf*rhof;
mu = 0.0215;
r1 = 1.0/2.0;
r2 = 1.4/2.0;
Urel = 2;
U1 = 1.0;
U2 = -1.0;
cutinner = 1.001*(r1+r2);
cutouter = 1.05*(r1+r2);

folder = '/tmp/ranga/two_particles/';

filename = strcat(folder,'log.lammps');
filename2 = strcat(folder,'dump.lammpstrj');

tdump = 1;
N = 2500; %Number of steps
N2 = 2500;
Nfgets = 115;

nPart = 2;


pID = 2; % Particle ID
forceplot = 1; % Which force according to flag in pairstyle is plotted






%% Read in s.p

display('Reading particle output file');

f1 = 'timestep'; v1 = zeros(1,1);
f2 = 'natoms'; v2 = zeros(1,1);
f3 = 'xbox'; v3 = zeros(1,3);
f4 = 'ybox'; v4 = zeros(1,3);
f5 = 'zbox'; v5 = zeros(1,3);
f6 = 'values'; v6 = zeros(1,18);

pdata(1:N,1:nPart) = struct(f1, v1, f2, v2, f3, v3, f4, v4, f5, v5, f6, v6);


file = fopen(filename2);
for i=1:N
    pdata(i,1).timestep = fscanf(file, '%*s %*s\n%d\n', 1);
    pdata(i,1).natoms = fscanf(file, '%*s %*s %*s %*s\n%d\n', 1);
     pdata(i,1).xbox = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f\n', [1, 3]);
     pdata(i,1).ybox = fscanf(file, '%f %f %f\n', [1, 3]);
     pdata(i,1).zbox = fscanf(file, '%f %f %f\n', [1, 3]);
    for j=1:pdata(i,1).natoms
        if j == 1
            %dump id2 all custom 100 s.p id type radius mass x y z vx vy vz fx fy fz tqx tqy tqz omegax omegay omegaz
            tmp = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        else
            tmp = fscanf(file, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        end
        pdata(i,tmp(1)).values = tmp;       
    end
end
fclose(file);

%% Non-dimensionialisation and computation of analytical lubrication force for comparison 
display('Non-dimensionalisation and computation of analytical solution for lubrication force')

r1 = pdata(1,1).values(3);
r2 = pdata(1,2).values(3);

ccdx21 = zeros(N,1); ccdy21 = zeros(N,1); ccd21 = zeros(N,1);
nx21 = zeros(N,1); ny21 = zeros(N,1);

for i=1:N
    % Center-to-center distance
    ccdx21(i) = pdata(i,2).values(5) - pdata(i,1).values(5);
    ccdy21(i) = pdata(i,2).values(6) - pdata(i,1).values(6);
    ccd21(i) = sqrt(ccdx21(i).^2 + ccdy21(i).^2);
    nx21(i)=ccdx21(i)/ccd21(i);
    ny21(i)=ccdy21(i)/ccd21(i);
end



% Separation value s
eps21 = ccd21 - r1 - r2;

r12= r1.*r2./(r1+r2);

% Dimensionless gap distance
hsep = eps21./r12;

hcutin=(cutinner- r1 - r2)/r12;
hcutout=(cutouter- r1 - r2)/r12;

U1 = zeros(N,1); U2 = zeros(N,1); U3 = zeros(N,1); 
U1nx = zeros(N,1); U2nx = zeros(N,1); Usep = zeros(N,1); Utsep = zeros(N,1); 
U1ny = zeros(N,1); U2ny = zeros(N,1); 
U1n = zeros(N,1); U2n = zeros(N,1);
U1tx = zeros(N,1); U2tx = zeros(N,1);
U1ty = zeros(N,1); U2ty = zeros(N,1);
U1t = zeros(N,1); U2t = zeros(N,1);
F1x = zeros(N,1); F1y = zeros(N,1);
F2x = zeros(N,1); F2y = zeros(N,1);
%Omega1 = zeros(N,1); Omega2 = zeros(N,1);
for i=1:N
    % Constant relative particle velocity (here assumed to be the same for both)
    U1(i) = sqrt(pdata(i,1).values(8).^2 + pdata(i,1).values(9).^2);
    U2(i) = sqrt(pdata(i,2).values(8).^2 + pdata(i,2).values(9).^2);
    
    U1x(i) = pdata(i,1).values(8);
    U1y(i) = pdata(i,1).values(9);
    U2x(i) = pdata(i,2).values(8);
    U2y(i) = pdata(i,2).values(9);
    F1x(i)= pdata(i,1).values(11);
    F2x(i)= pdata(i,2).values(11);
    F1y(i)= pdata(i,1).values(12);
    F2y(i)= pdata(i,2).values(12);
    
    U1nx(i) = (nx21(i).*U1x(i) + ny21(i).*U1y(i)).*nx21(i);
    U1ny(i) = (nx21(i).*U1x(i) + ny21(i).*U1y(i)).*ny21(i);
    U2nx(i) = (nx21(i).*U2x(i) + ny21(i).*U2y(i)).*nx21(i);
    U2ny(i) = (nx21(i).*U2x(i) + ny21(i).*U2y(i)).*ny21(i);
    
    U1n(i) = sqrt(U1nx(i)^2 + U1ny(i)^2);
    U2n(i) = sqrt(U2nx(i)^2 + U2ny(i)^2);
    
    Usep(i)=sqrt((U1nx(i)-U2nx(i)).^2+(U1ny(i)-U2ny(i)).^2);
    
    U1tx(i) = U1x(i) - U1nx(i);
    U1ty(i) = U1y(i) - U1ny(i);
    U2tx(i) = U2x(i) - U2nx(i);
    U2ty(i) = U2y(i) - U2ny(i);
    
    U1t(i) = sqrt(U1tx(i)^2 + U1ty(i)^2);
    U2t(i) = sqrt(U2tx(i)^2 + U2ty(i)^2);
    
     Utsep(i)=sqrt((U1tx(i)-U2tx(i)).^2+(U1ty(i)-U2ty(i)).^2);
end



display('Non-dimensionalisation of forces and calculation of error')
% Make particle forces dimensionless
f_dimensionless = zeros(N,1);
f_analytical = zeros(N,1);
f_analytical_dimensionless = zeros(N,1);
errorn = zeros(N,1);
errort = zeros(N,1);
flubanalyticaln = zeros(N,1);
flubanalyticalt = zeros(N,1);

F_l_nx=zeros(N,1);F_l_ny=zeros(N,1);
F_l_tx=zeros(N,1);F_l_ty=zeros(N,1);

F_fac=(4.0/15.0)*( 1 +r12/(r1+r2) + ((r1-r2)/(r1+r2)).^2 );

%SC_c_xy=zeros(N,1);
SC_l_xy=zeros(N,1);%SC_t_xy=zeros(N,1);
SC_l_yx=zeros(N,1);
SC_l=zeros(N,1);

for i = 1:N %size(s,2)


  f_dimensionless(i) =sqrt( F1x(i)^2 + F1y(i)^2 );

  
    if hsep(i)<=hcutin
        F_l_nx(i) =  (U1nx(i)-U2nx(i))/hcutin;
        F_l_tx(i) = F_fac*(U1tx(i)-U2tx(i))*log(1.0/hcutin);
        F_l_ny(i) =  (U1ny(i)-U2ny(i))/hcutin;
        F_l_ty(i) = F_fac*(U1ty(i)-U2ty(i))*log(1.0/hcutin);
    elseif hsep(i)<=hcutout
        F_l_nx(i) =  (U1nx(i)-U2nx(i))/hsep(i);
        F_l_tx(i) = F_fac*(U1tx(i)-U2tx(i))*log(1.0/hsep(i));
        F_l_ny(i) =  (U1ny(i)-U2ny(i))/hsep(i);
        F_l_ty(i) = F_fac*(U1ty(i)-U2ty(i))*log(1.0/hsep(i));
    else 
        F_l_nx(i) =  0;
        F_l_tx(i) = 0;
        F_l_ny(i) =  0;
        F_l_ty(i) = 0;    
    end
    ffa1=6*pi*mu*r12;  
   SC_l_xy(i)=-ffa1*(F_l_tx(i)+F_l_nx(i))*(pdata(i,1).values(6)-pdata(i,2).values(6));
   SC_l_yx(i)=-ffa1*(F_l_ty(i)+F_l_ny(i))*(pdata(i,1).values(5)-pdata(i,2).values(5));
   SC_l(i)=F1x(i)*(pdata(i,1).values(6)-pdata(i,2).values(6));
                        
    % Squeezing force
    flubanalyticaln(i) = -sqrt((F_l_nx(i)+F_l_tx(i)).^2+(F_l_ny(i)+F_l_ty(i)).^2)/U1n(i);
    f_dimensionless(i) = f_dimensionless(i)/(6.0*pi*r12*U1n(i)*mu);

    

    

   if hsep(i)>hcutout
    % Error calculation
     errorn(i) = abs((abs(flubanalyticaln(i)) - abs(f_dimensionless(i))))/abs(flubanalyticaln(i))*100;
    % errort(i) = abs((abs(flubanalyticalt(i)) - abs(f_dimensionless(i))))/abs(flubanalyticalt(i))*100;
   else
      errorn(i) =  0;
      errort(i) = 0;
   end

end


%% Plots
display('Plotting');


if(forceplot == 1)
    loglog(hsep, f_dimensionless, '-o','LineWidth', 2);
    hold on;
    loglog(hsep, -flubanalyticaln, 'r','linewidth', 2)
end

if(forceplot == 2)

  
 loglog(hsep, SC_l_xy, '-k','LineWidth', 2);
 hold on;
 loglog(hsep,SC_l, '-r','LineWidth', 2);
 loglog(hsep, SC_l_yx, '-b','LineWidth', 2);
end

%title('Force over gap distance compared to analytical solution')
xlabel('Gap distance (dimensionless - related to particle 1)')
ylabel('Force/Torque (dimensionless - related to particle 1')
hold off;

