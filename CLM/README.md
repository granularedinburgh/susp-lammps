Implemented the Critical load model based on Seto et al.'s work from [Phys. Rev. Lett.](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.111.218301). The base code is the granular pair style in LAMMPS.

There is an input script and a couple of MATLAB scripts to test the implementation.