%-------------------------------------------------------------------------%
% Script to verify stress contributions due to multiple particles
% Date May 1, 2017
% Modified by Ranga for the new lubrication class Simple
%-------------------------------------------------------------------------%

clear;
%close all;

display('Reading parameters');
%muf = nuf*rhof;
mu = 1; %FLD_VISC


Kn=10000;
Kt=2*Kn/7.0;

folder = '/tmp/ranga/two_particles/CLM/';
filename2 = strcat(folder,'dump.lammpstrj');

gdot =0.0; % 0.05;

N = 100; %Number of steps

nPart = 2;


dt=1e-4;

shx_new=0;shy_new=0;shz_new=0;
load_cutoff=3.0;
xmu=0.1;


%% Read in s.p

display('Reading particle output file');

f1 = 'timestep'; v1 = zeros(1,1);
f2 = 'natoms'; v2 = zeros(1,1);
f3 = 'xbox'; v3 = zeros(1,3);
f4 = 'ybox'; v4 = zeros(1,3);
f5 = 'zbox'; v5 = zeros(1,3);
f6 = 'values'; v6 = zeros(1,19);


pdata(1:N,1:nPart) = struct(f1, v1, f2, v2, f3, v3, f4, v4, f5, v5, f6, v6);


file = fopen(filename2);

for i=1:N
    pdata(i,1).timestep = fscanf(file, '%*s %*s\n%d\n', 1);
    pdata(i,1).natoms = fscanf(file, '%*s %*s %*s %*s\n%d\n', 1);
    pdata(i,1).xbox = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f\n', [1, 3]);
    pdata(i,1).ybox = fscanf(file, '%f %f %f\n', [1, 3]);
    pdata(i,1).zbox = fscanf(file, '%f %f %f\n', [1, 3]);
    
    for j=1:pdata(i,1).natoms
        if j == 1
            %dump id2 all custom 100 s.p id type radius mass x y z vx vy vz fx fy fz tqx tqy tqz omegax omegay omegaz
            tmp = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        else
            tmp = fscanf(file, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        end
        pdata(i,tmp(1)).values = tmp;
        
    end
    
end
fclose(file);


%% Some Variable declaration
display('Declaring variables');
fytot=zeros([N nPart]);
fylub=zeros([N nPart]);
fyfric=zeros([N nPart]);

xpos=zeros([N nPart]);
ypos=zeros([N nPart]);
zpos=zeros([N nPart]);

Ux=zeros([N nPart]);
Uy=zeros([N nPart]);
Uz=zeros([N nPart]);

Fx=zeros([N nPart]);
Fy=zeros([N nPart]);
Fz=zeros([N nPart]);
type=zeros([N nPart]);
time=zeros([N 1]);
f_dimensionless = zeros(N,1);
fanalyticaln = zeros(N,1);

torx = zeros(N,1);
tory = zeros(N,1);
torz = zeros(N,1);

toranalx=zeros(N,1);
toranaly=zeros(N,1);
toranalz=zeros(N,1);

for i = 1:N %size(s,2)
    for j=1:pdata(i,1).natoms
        xpos(i,j)=pdata(i,j).values(5);
        ypos(i,j)=pdata(i,j).values(6);
        zpos(i,j)=pdata(i,j).values(7);
        
        type(i,j)=pdata(i,j).values(2);
        
        Ux(i,j)=pdata(i,j).values(8);
        Uy(i,j)=pdata(i,j).values(9);
        Uz(i,j)=pdata(i,j).values(10);
        
        Fx(i,j)=pdata(i,j).values(11);
        Fy(i,j)=pdata(i,j).values(12);
        Fz(i,j)=pdata(i,j).values(13);
    end
    time(i)=dt*pdata(i,1).timestep;
end



lx = pdata(1,1).xbox(2)- pdata(1,1).xbox(1);
ly = pdata(1,1).ybox(2)- pdata(1,1).ybox(1);
lz = pdata(1,1).zbox(2)- pdata(1,1).zbox(1);
box_vol=lx*ly*lz;

initial=dt*0;
delx = (time-initial)*gdot*ly; % assume shear rate = dvx/dy box displacement
%delx = disp; % -L/2 <delx < L/2
delv = gdot*ly;

Scontxy=zeros(N,1);
Slubxy=zeros(N,1);

shx=0;
shy=0;
shz=0;


%% Computation of force for comparison
display('Computing forces')
fn_arr=[];
fs_arr=[];
for i = 1:N %size(s,2)
    %f_dimensionless(i) =sqrt(Fx(i,1).^2+Fy(i,1).^2+Fz(i,1).^2);
    f_dimensionless(i) =Fy(i,1);
    torx(i)=pdata(i,1).values(14);
    tory(i)=pdata(i,1).values(15);
    torz(i)=pdata(i,1).values(16);
    for j=1:pdata(i,1).natoms
        r1=pdata(i,j).values(3);
        
        for k=j+1:pdata(i,1).natoms
            r2=pdata(i,k).values(3);
            %calculate contact forces
            
            [ccdx21,ccdy21,ccdz21,ccd21]=minimage(lx,ly,lz,delx(i),xpos(i,j),ypos(i,j),zpos(i,j),xpos(i,k),ypos(i,k),zpos(i,k));
            
            nx=ccdx21/ccd21;
            ny=ccdy21/ccd21;
            nz=ccdz21/ccd21;
            eps21 = ccd21 - r1 - r2;
            if eps21<0
                fx1=Kn*eps21*nx;
                fy1=Kn*eps21*ny;
                fz1=Kn*eps21*nz;
            else
                fx1=0;
                fy1=0;
                fz1=0;
            end
            fn=sqrt(fx1^2+fy1^2+fz1^2);
            fn_arr=[fn_arr fn];
            
                        
            Vx=min_velx(ypos(i,j),ypos(i,k),Ux(i,j),Ux(i,k),ly,delv);
            Vy=Uy(i,k)-Uy(i,j);
            Vz=Uz(i,k)-Uz(i,j);
            
            
            Vnnr=(nx*Vx + ny*Vy+nz*Vz);
            Unx = Vnnr.*nx;
            Uny = Vnnr.*ny;
            Unz=  Vnnr.*nz;
            
            Utx = Vx - Unx;
            Uty = Vy - Uny;
            Utz = Vz - Unz;
            
                           
            
            Omg1x= pdata(i,j).values(17);
            Omg1y= pdata(i,j).values(18);
            Omg1z= pdata(i,j).values(19);
            
            Omg2x= pdata(i,k).values(17);
            Omg2y= pdata(i,k).values(18);
            Omg2z= pdata(i,k).values(19);
            
            wrx=(Omg1x*r1+Omg2x*r2)/ccd21;
            wry=(Omg1y*r1+Omg2y*r2)/ccd21;
            wrz=(Omg1z*r1+Omg2z*r2)/ccd21;
            
            Utrx=Utx-(ccdz21*wry-ccdy21*wrz);
            Utry=Uty-(ccdx21*wrz-ccdz21*wrx);
            Utrz=Utz-(ccdy21*wrx-ccdx21*wry);
            
            Utr=sqrt(Utrx*Utrx+Utry*Utry+Utrz*Utrz);
            shx=shx_new;
            shy=shy_new;
            shz=shz_new;
            
            shx_new=shx+Utrx*dt;
            shy_new=shy+Utry*dt;
            shz_new=shz+Utrz*dt;
            
            shrmag=sqrt(shx^2+shy^2+shz^2);
            
            %Rotate shear displacement 
            rsht=(shx*ccdx21+shy*ccdy21+shz*ccdz21)/ccd21^2;
            shx=shx-rsht*ccdx21;
            shy=shy-rsht*ccdy21;
            shz=shz-rsht*ccdz21;
             % tangential forces = shear + tangential velocity damping

            
            %fn=abs(Kn*eps21);
            if  fn> load_cutoff
            fn=xmu*(fn-load_cutoff);
             fsx =   Kt*shx;
             fsy =   Kt*shy;
             fsz =  Kt*shz;
              fs = sqrt(fsx^2+ fsy^2 + fsz^2);           
            else
             fs=0;fsx=0;fsy=0;fsz=0;
            end
            
         % rescale frictional displacements and forces if needed
        if (fs > fn) 
          if (shrmag ~= 0.0) 
            shx = (fn/fs) * shx;
            shy = (fn/fs) * shy;
            shz = (fn/fs) * shz;
            fsx = fn/fs*fsx;
            fsy = fn/fs*fsy;
            fsz = fn/fs*fsz;
            fs = sqrt(fsx^2+ fsy^2 + fsz^2);
           else
              fs=0.0; fsx = 0.0; fsy = 0.0; fsz = 0.0;
          end
        end
        fs_arr=[fs_arr fs];
        
        % forces & torques

        fx = fx1 + fsx;
        fy = fy1 + fsy;
        fz = fz1 + fsz;
        
        T_l_nx =  -(ccdy21*fsz - ccdz21*fsy)/ccd21;
        T_l_ny = -(ccdz21*fsx - ccdx21*fsz)/ccd21;
        T_l_nz = -(ccdx21*fsy - ccdy21*fsx)/ccd21;

   
        %fanalyticaln(i)=sqrt((fx).^2+(fy).^2+(fz).^2);
         fanalyticaln(i)=fsy;   
            toranalx(i)=T_l_nx*r1;
            toranaly(i)=T_l_ny*r1;
            toranalz(i)=T_l_nz*r1;
       %Sum stress contribution due to contact forces
        Scontxy(i)=Scontxy(i)+fsy*ccdx21;
            
        end
        
    end
    
end


for i = 1:N %size(s,2)
    
    Scontxy(i)=Scontxy(i)/box_vol;
    Slubxy(i)=Slubxy(i)/box_vol;
end



%% Plotting of solution

display('Plot of analytical solution for stress')


    subplot(3,1,1);
   plot(time(2:end), f_dimensionless(2:end), 'o','LineWidth', 2);
    hold on;
    plot(time, fanalyticaln, '-r','linewidth', 2);
    
    %xlabel('Time');
    ylabel('Force');
    set(gca,'XScale','log');
    hold off;
    
    
    subplot(3,1,2);
    plot(time,sqrt(toranalx.^2+toranaly.^2+toranalz.^2), '-b','LineWidth', 2);
    hold on;
     plot(time,sqrt(torx.^2+tory.^2+torz.^2), 'xb','LineWidth', 2);
    %xlabel('Time');
    ylabel('Torque');
    set(gca,'XScale','log');
    hold off;
    
    %plot(time,-SC_t, '-k','LineWidth', 2);
    
    
    %plot(time,-SC_con, '-r','LineWidth', 2);
    subplot(3,1,3);


sdata=dlmread(strcat(folder,'stressOutputKE'), ' ', 1, 0);
plot(time(2:end),Scontxy(2:end), '-b','LineWidth', 2);
hold all;
% plot(time(2:end),Scontxy(2:end), '-r','LineWidth', 2);
% plot(time(2:end),Slubxy(2:end)+Scontxy(2:end), '-k','LineWidth', 2);
% 
plot(sdata(:,1)*dt,-sdata(:,2), 'ok','LineWidth', 2);
% 
% plot(sdata(:,1)*dt,-sdata(:,3), 'or','LineWidth', 2);
% plot(sdata(:,1)*dt,-sdata(:,4), 'ob','LineWidth', 2);
% 
% 
set(gca,'XScale','log');
xlabel('Time');
ylabel('Stress');
hold off;
% 
figure();
plot(fn_arr,fs_arr);
hold all;
plot(fn_arr,(fn_arr-load_cutoff)*xmu,'--k');

hold off;

plot(fn_arr);
hold all;
plot(fs_arr/xmu+load_cutoff);
hold off;

