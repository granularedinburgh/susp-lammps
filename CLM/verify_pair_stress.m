%-------------------------------------------------------------------------%
% Script to analyse/verify pair style resistance_matrix_flags
% in LAMMPS/LIGGGHTS
% Simulation results between two particles are compared to the analytical
% solution (/DEM formula)
%
%
% Original Author: Tim Najuch, 3/6/2016
% Modified by: Ranga Radhakrishnan 10/4/2017
% Latest Modification May 30/05
%-------------------------------------------------------------------------%

function [] = verify_pair_stress()
clear;
close all;

%% Parameters of case
%
display('Reading parameters');

rhos = 1;
rhof = 1;
nuf = 1;
%muf = nuf*rhof;
mu = 0.215;
r1 = 1.0/2.0;
r2 = 1.4/2.0;
Urel = 2;
U1 = 0.1;
U2 = -0.1;
cutinner = 1.001*(r1+r2);
cutouter = 1.05*(r1+r2);
Kn=10000;
box_vol=20.0^3;

folder = '/tmp/ranga/two_particles/';

filename = strcat(folder,'log.lammps');
filename2 = strcat(folder,'dump.lammpstrj');

tdump = 1;
N = 50; %Number of steps
N2 = 50;
Nfgets = 115;

nPart = 2;


pID = 2; % Particle ID
forceplot =2; % Which force according to flag in pairstyle is plotted

dt=0.0001;




%% Read in s.p

display('Reading particle output file');

f1 = 'timestep'; v1 = zeros(1,1);
f2 = 'natoms'; v2 = zeros(1,1);
f3 = 'xbox'; v3 = zeros(1,3);
f4 = 'ybox'; v4 = zeros(1,3);
f5 = 'zbox'; v5 = zeros(1,3);
f6 = 'values'; v6 = zeros(1,19);


pdata(1:N,1:nPart) = struct(f1, v1, f2, v2, f3, v3, f4, v4, f5, v5, f6, v6);



file = fopen(filename2);


for i=1:N
    pdata(i,1).timestep = fscanf(file, '%*s %*s\n%d\n', 1);
    pdata(i,1).natoms = fscanf(file, '%*s %*s %*s %*s\n%d\n', 1);
    pdata(i,1).xbox = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f\n', [1, 3]);
    pdata(i,1).ybox = fscanf(file, '%f %f %f\n', [1, 3]);
    pdata(i,1).zbox = fscanf(file, '%f %f %f\n', [1, 3]);
    
    for j=1:pdata(i,1).natoms
        if j == 1
            %dump id2 all custom 100 s.p id type radius mass x y z vx vy vz fx fy fz tqx tqy tqz omegax omegay omegaz
            tmp = fscanf(file, '%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s\n%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
        else
            tmp = fscanf(file, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', [1 19]);
            
        end
        pdata(i,tmp(1)).values = tmp;
        
    end
    
end
fclose(file);

%% Non-dimensionialisation and computation of analytical lubrication force for comparison
display('Non-dimensionalisation and computation of analytical solution for lubrication force')

r1 = pdata(1,1).values(3);
r2 = pdata(1,2).values(3);

lx = pdata(1,1).xbox(2)- pdata(1,1).xbox(1);
ly = pdata(1,1).ybox(2)- pdata(1,1).ybox(1);
lz = pdata(1,1).zbox(2)- pdata(1,1).zbox(1);


ccdx21 = zeros(N,1); ccdy21 = zeros(N,1);ccdz21 = zeros(N,1);  ccd21 = zeros(N,1);
nx21 = zeros(N,1); ny21 = zeros(N,1); nz21 = zeros(N,1);

for i=1:N
    % Center-to-center distance
    %     ccdx21(i) = pdata(i,2).values(5) - pdata(i,1).values(5);
    %     ccdy21(i) = pdata(i,2).values(6) - pdata(i,1).values(6);
    %     ccdz21(i) = pdata(i,2).values(7) - pdata(i,1).values(7);
    %     ccd21(i) = sqrt(ccdx21(i).^2 + ccdy21(i).^2+ccdz21(i).^2);
    [ccdx21(i),ccdy21(i),ccdz21(i),ccd21(i)]=minimage(lx,ly,lz,0,pdata(i,1).values(5),pdata(i,1).values(6),pdata(i,1).values(7),pdata(i,2).values(5),pdata(i,2).values(6),pdata(i,2).values(7));
    nx21(i)=ccdx21(i)/ccd21(i);
    ny21(i)=ccdy21(i)/ccd21(i);
    nz21(i)=ccdz21(i)/ccd21(i);
end



% Separation value s
eps21 = ccd21 - r1 - r2;

r12= r1.*r2./(r1+r2);

% Dimensionless gap distance
hsep = eps21./r12;

%hcutin=(cutinner- r1 - r2)/r12;
%hcutout=(cutouter- r1 - r2)/r12;


U1x= zeros(N,1); U2x= zeros(N,1); 
U1y= zeros(N,1); U2y= zeros(N,1); 
U1z= zeros(N,1); U2z= zeros(N,1);
F1x = zeros(N,1); F1y = zeros(N,1);
F2x = zeros(N,1); F2y = zeros(N,1);
Omg1x = zeros(N,1); Omg2x = zeros(N,1);
Omg1y = zeros(N,1); Omg2y = zeros(N,1);
Omg1z = zeros(N,1); Omg2z = zeros(N,1);
Torz=zeros(N,1);

time=zeros(N,1);

for i=1:N
    % Constant relative particle velocity (here assumed to be the same for both)

    U1x(i) = pdata(i,1).values(8);
    U1y(i) = pdata(i,1).values(9);
    U1z(i) = pdata(i,1).values(10);
    
    U2x(i) = pdata(i,2).values(8);
    U2y(i) = pdata(i,2).values(9);
    U2z(i) = pdata(i,2).values(10);
    
    F1x(i)= pdata(i,1).values(11);
    F2x(i)= pdata(i,2).values(11);
    F1y(i)= pdata(i,1).values(12);
    F2y(i)= pdata(i,2).values(12);
    
    Omg1x(i)= pdata(i,1).values(17);
    Omg2x(i)= pdata(i,2).values(17);
    Omg1y(i)= pdata(i,1).values(18);
    Omg2y(i)= pdata(i,2).values(18);
    Omg1z(i)= pdata(i,1).values(19);
    Omg2z(i)= pdata(i,2).values(19);   
    
    time(i)=dt*pdata(i,1).timestep;

    
    
    
end



display('Non-dimensionalisation of forces and calculation of error')
% Make particle forces dimensionless
f_dimensionless = zeros(N,1);
f_analytical = zeros(N,1);
f_analytical_dimensionless = zeros(N,1);
errorn = zeros(N,1);
errort = zeros(N,1);
flubanalyticaln = zeros(N,1);
flubanalyticalt = zeros(N,1);

ffricanalyticaln = zeros(N,1);
%flubanalyticalt = zeros(N,1);

F_l_nx=zeros(N,1);F_l_ny=zeros(N,1);
F_l_tx=zeros(N,1);F_l_ty=zeros(N,1);

F_f_nx=zeros(N,1);F_f_ny=zeros(N,1);

F_fac=(4.0/15.0)*( 1 +r12/(r1+r2) + ((r1-r2)/(r1+r2)).^2 );

%SC_c_xy=zeros(N,1);
SC_l=zeros(N,1);%SC_t_xy=zeros(N,1);
SC_t=zeros(N,1);
SC_f=zeros(N,1);

Scomp_l=zeros(N,1);%SC_t_xy=zeros(N,1);
Scomp_f=zeros(N,1);
Scomp_t=zeros(N,1);


gdot = 1.0;
disp = time*gdot*ly; % assume shear rate = dvx/dy box displacement
delx = disp - round(disp/lx)*lx; % -L/2 <delx < L/2
delv = gdot*ly;


ffa1=6*pi*mu*r12;
F_fac=pi*mu*r12*(8.0/5.0)*( 1 +r12/(r1+r2) + ((r1-r2)/(r1+r2)).^2 );
for i = 1:N %size(s,2)
    
    
    f_dimensionless(i) =sqrt( F1x(i)^2 + F1y(i)^2 );
    
    r12= r1.*r2./(r1+r2);
    
    hcutin=0.001*(r1+r2)/r12;
    hcutout=0.05*(r1+r2)/r12;
    
    U1x(i)=U1x(i) + r1*(Omg1y(i)*nz21(i)-Omg1z(i)*ny21(i));
    U1y(i)=U1y(i) - r1*(Omg1x(i)*nz21(i)-Omg1z(i)*nx21(i));
    U1z(i)=U1z(i) - r1*(Omg1y(i)*nx21(i)-Omg1x(i)*ny21(i));
    
    U2x(i)=U2x(i) - r2*(Omg2y(i)*nz21(i)-Omg2z(i)*ny21(i));
    U2y(i)=U2y(i) + r2*(Omg2x(i)*nz21(i)-Omg2z(i)*nx21(i));
    U2z(i)=U2z(i) + r2*(Omg2y(i)*nx21(i)-Omg2x(i)*ny21(i));
    
    Vx=min_velx(pdata(i,1).values(6),pdata(i,2).values(6),U1x(i),U2x(i),ly,delv);
    %Vx=U1x(i)-U2x(i);
    Vy=U1y(i)-U2y(i);
    Vz=U1z(i)-U2z(i);
    Unx = (nx21(i).*Vx + ny21(i).*Vy + nz21(i).*Vz).*nx21(i);
    Uny = (nx21(i).*Vx + ny21(i).*Vy + nz21(i).*Vz).*ny21(i);
    Utx = Vx - Unx;
    Uty = Vy - Uny;
    
    
    if hsep(i)<=hcutin
        F_l_nx(i) =  ffa1*Unx./hcutin;
        F_l_tx(i) = F_fac*Utx*log(1.0/hcutin);
        F_l_ny(i) =  ffa1*Uny/hcutin;
        F_l_ty(i) = F_fac*Uty*log(1.0/hcutin);
    elseif hsep(i)<=hcutout
        F_l_nx(i) =  ffa1*Unx/hsep(i);
        F_l_tx(i) = F_fac*Utx*log(1.0/hsep(i));
        F_l_ny(i) =  ffa1*Uny/hsep(i);
        F_l_ty(i) = F_fac*Uty*log(1.0/hsep(i));
    else
        F_l_nx(i) =  0;
        F_l_tx(i) = 0;
        F_l_ny(i) =  0;
        F_l_ty(i) = 0;
    end
    
    %%add_force=ffa/r12*(r1*(U1y
    
    
    
    %     if hsep(i)<=hcutin
    %         F_l_nx(i) =  (U1nx(i)-U2nx(i))/hcutin;
    %         F_l_tx(i) = F_fac*(U1tx(i)-U2tx(i))*log(1.0/hcutin);
    %         F_l_ny(i) =  (U1ny(i)-U2ny(i))/hcutin;
    %         F_l_ty(i) = F_fac*(U1ty(i)-U2ty(i))*log(1.0/hcutin);
    %     elseif hsep(i)<=hcutout
    %         F_l_nx(i) =  (U1nx(i)-U2nx(i))/hsep(i);
    %         F_l_tx(i) = F_fac*(U1tx(i)-U2tx(i))*log(1.0/hsep(i));
    %         F_l_ny(i) =  (U1ny(i)-U2ny(i))/hsep(i);
    %         F_l_ty(i) = F_fac*(U1ty(i)-U2ty(i))*log(1.0/hsep(i));
    %     else
    %         F_l_nx(i) =  0;
    %         F_l_tx(i) = 0;
    %         F_l_ny(i) =  0;
    %         F_l_ty(i) = 0;
    %     end
    
    if eps21(i)<=0
        F_f_nx(i)=Kn*(eps21(i))*nx21(i);
        F_f_ny(i)=Kn*(eps21(i))*ny21(i);
    end
    
    
    
    
    fy2=(F_l_ny(i)+F_l_ty(i));
    
    SC_l(i)=fy2*ccdx21(i);
    SC_t(i)=-F1y(i)*ccdx21(i);
    
    SC_f(i)=-F_f_ny(i)*ccdx21(i);
    
    Torz(i)=ccdx21(i)*fy2-ccdy21(i)*(F_l_nx(i)+F_l_tx(i));
    % Squeezing force
    flubanalyticaln(i) = -sqrt((F_l_nx(i)+F_l_tx(i)).^2+(F_l_ny(i)+F_l_ty(i)).^2);
    % f_dimensionless(i) = f_dimensionless(i)/(6.0*pi*r12*U1n(i)*mu);
    
    
    
    
    if hsep(i)>hcutout
        % Error calculation
        errorn(i) = abs((abs(flubanalyticaln(i)) - abs(f_dimensionless(i))))/abs(flubanalyticaln(i))*100;
        % errort(i) = abs((abs(flubanalyticalt(i)) - abs(f_dimensionless(i))))/abs(flubanalyticalt(i))*100;
    else
        errorn(i) =  0;
        errort(i) = 0;
    end
    
end


%% Plots
display('Plotting');


if(forceplot == 1)
    loglog(time, f_dimensionless, 'o','LineWidth', 2);
    hold on;
    loglog(time, -flubanalyticaln, 'r','linewidth', 2);
    %loglog(hsep, F_f_nx, '--k','linewidth', 2);
    %loglog(hsep, F_f_ny, '-k','linewidth', 2);
end

if(forceplot == 2)
    
    
    %sdata=dlmread('stressOutputKE', ' ', 1, 0);
    sdata=dlmread('stressOutputlub', ' ', 1, 0);
    plot( time(2:end),-(SC_f(2:end)+SC_l(2:end))/box_vol, 'ok','LineWidth', 2);
    hold on;
    %plot(time,-SC_l/box_vol, '-r','LineWidth', 2);
    plot(time(2:end),-SC_f(2:end)/box_vol, 'or','LineWidth', 2);
    plot(time(2:end),-SC_l(2:end)/box_vol, 'ob','LineWidth', 2);
    
    plot(sdata(2:end,1)*dt,-sdata(2:end,2), '--k','LineWidth', 2);
    % plot(sdata(:,1)/100,-sdata(:,4), '--r','LineWidth', 2);
        plot(sdata(:,1)*dt,-sdata(:,3), '--r','LineWidth', 2);
        plot(sdata(:,1)*dt,-sdata(:,4), '--b','LineWidth', 2);
    
end

%title('Force over gap distance compared to analytical solution')
xlabel('Time')
ylabel('Force/Torque (dimensionless - related to particle 1')
hold off;

end

%%

function [rx,ry,rz,del]=minimage(lx,ly,lz,delx,px1,py1,pz1,px2,py2,pz2)

rx = px2 - px1;
ry = py2 - py1;
rz = pz2 - pz1;

cory = round(ry / ly); % assume shear rate = dvx/dy
rx = rx - cory * delx;
rx = rx - round(rx/lx) * lx;
ry = ry - cory * ly;
rz = rz - round(rz/lz) * lz;



del = sqrt(rx.*rx + ry.*ry + rz.*rz);

end


function [dvx]=min_velx(y1,y2,vx1,vx2,ly,delv)
%minimum image for velocity x

dy = y1-y2;

cory = round(dy / ly); % assume shear rate = dvx/dy
dvx = vx1 -(vx2 +cory * delv);

end
