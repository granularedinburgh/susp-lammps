/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "compute_cutoff_atom.h"
#include "atom.h"
#include "update.h"
#include "modify.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "force.h"
#include "pair.h"
#include "comm.h"
#include "memory.h"
#include "error.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeCutoffAtom::ComputeCutoffAtom(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg),
  contact(NULL)
{
  if (narg != 4) error->all(FLERR,"Illegal compute cutoff/atom command");

  cutoff  = force->numeric(FLERR,arg[3]);
  if (cutoff < 0.0 ) error->all(FLERR,"Illegal cutoff/atom command");

  peratom_flag = 1;
  size_peratom_cols = 2;
  comm_reverse = 1;

  nmax = 0;

  // error checks

  if (!atom->sphere_flag)
    error->all(FLERR,"Compute cutoff/atom requires atom style sphere");
}

/* ---------------------------------------------------------------------- */

ComputeCutoffAtom::~ComputeCutoffAtom()
{
  memory->destroy(contact);
}

/* ---------------------------------------------------------------------- */

void ComputeCutoffAtom::init()
{
  if (force->pair == NULL)
    error->all(FLERR,"Compute contact/atom requires a pair style be defined");

  int count = 0;
  for (int i = 0; i < modify->ncompute; i++)
    if (strcmp(modify->compute[i]->style,"cutoff/atom") == 0) count++;
  if (count > 1 && comm->me == 0)
    error->warning(FLERR,"More than one compute cutoff/atom");

  // need an occasional neighbor list

  int irequest = neighbor->request(this,instance_me);
  neighbor->requests[irequest]->half = 0;
  neighbor->requests[irequest]->gran = 1;
  neighbor->requests[irequest]->pair = 0;
  neighbor->requests[irequest]->compute = 1;
  neighbor->requests[irequest]->occasional = 1;
}

/* ---------------------------------------------------------------------- */

void ComputeCutoffAtom::init_list(int id, NeighList *ptr)
{
  list = ptr;
}

/* ---------------------------------------------------------------------- */

void ComputeCutoffAtom::compute_peratom()
{
  int i,j,ii,jj,inum,jnum;
  double xtmp,ytmp,ztmp,delx,dely,delz,rsq;
  double radi,radsum,radsumsq;
  int *ilist,*jlist,*numneigh,**firstneigh;

  invoked_peratom = update->ntimestep;

  // grow contact array if necessary

  if (atom->nmax > nmax) {
    memory->destroy(contact);
    nmax = atom->nmax;
    memory->create(contact,nmax,2,"cutoff/atom:cutoff");
    array_atom = contact;
  }

  // invoke neighbor list (will copy or build if necessary)

  neighbor->build_one(list);

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // compute number of contacts for each atom in group
  // contact if distance <= sum of radii
  // tally for both I and J

  double **x = atom->x;
  double *radius = atom->radius;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  int nall = nlocal + atom->nghost;

  for (i = 0; i < nall; i++) {
   contact[i][0] = 0.0;
   contact[i][1] = 0.0;
  }

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    if (mask[i] & groupbit) {
      xtmp = x[i][0];
      ytmp = x[i][1];
      ztmp = x[i][2];
      radi = radius[i];
      jlist = firstneigh[i];
      jnum = numneigh[i];

      for (jj = 0; jj < jnum; jj++) {
        j = jlist[jj];
        j &= NEIGHMASK;

        delx = xtmp - x[j][0];
        dely = ytmp - x[j][1];
        delz = ztmp - x[j][2];
        rsq = delx*delx + dely*dely + delz*delz;
        radsum = radi + radius[j];
        radsumsq = radsum*radsum;

	if (rsq <= radsumsq) {
          contact[i][0] += 1.0;
          contact[j][0] += 1.0;
	  double radcutsq=pow(radsum-cutoff,2.0);
	  if (rsq<=radcutsq){
          contact[i][1] += 1.0;
          contact[j][1] += 1.0;
	  }
        }

      }
    }
  }

  // communicate ghost atom counts between neighbor procs if necessary

  if (force->newton_pair) comm->reverse_comm_compute(this);
}

/* ---------------------------------------------------------------------- */

int ComputeCutoffAtom::pack_reverse_comm(int n, int first, double *buf)
{
  int i,m,last;

  m = 0;
  last = first + n;
  for (i = first; i < last; i++){
    buf[m++] = contact[i][0];
    buf[m++] = contact[i][1];
  }

  return m;
}

/* ---------------------------------------------------------------------- */

void ComputeCutoffAtom::unpack_reverse_comm(int n, int *list, double *buf)
{
  int i,j,m;

  m = 0;
  for (i = 0; i < n; i++) {
    j = list[i];
    contact[j][0] += buf[m++];
    contact[j][1] += buf[m++];
  }
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based array
------------------------------------------------------------------------- */

double ComputeCutoffAtom::memory_usage()
{
  double bytes = 2* nmax * sizeof(double);
  return bytes;
}
