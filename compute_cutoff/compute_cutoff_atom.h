/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
   Modified by Ranga Radhakrishnan (UoE) 12/11/2017
------------------------------------------------------------------------- */

#ifdef COMPUTE_CLASS

ComputeStyle(cutoff/atom,ComputeCutoffAtom)

#else

#ifndef LMP_COMPUTE_CUTOFF_ATOM_H
#define LMP_COMPUTE_CUTOFF_ATOM_H

#include "compute.h"

namespace LAMMPS_NS {

class ComputeCutoffAtom : public Compute {
 public:
  ComputeCutoffAtom(class LAMMPS *, int, char **);
  ~ComputeCutoffAtom();
  void init();
  void init_list(int, class NeighList *);
  void compute_peratom();
  int pack_reverse_comm(int, int, double *);
  void unpack_reverse_comm(int, int *, double *);
  double memory_usage();

 private:
  int nmax;
  class NeighList *list;
  double **contact;
  double cutoff;
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

E: Compute cutoff/atom requires atom style sphere

Self-explanatory.

E: Compute contact/atom requires a pair style be defined

Self-explantory.

W: More than one compute contact/atom

It is not efficient to use compute contact/atom more than once.

*/
