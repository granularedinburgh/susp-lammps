# Postprocessing programs written in cpp
### build programs using shared libraries for postprocessing

The programs that can be built by cmake are:
* cntct\_calc.cpp : Calculates contact numbers from dump file, 
* cntct\_forcedump.cpp : Calculates contact numbers from the force dump file (force dump file is in text format),
* dump\_pov.cpp: Outputs pov files in a folder **pov\_files** by reading only the dump file (bin or text file), and
* forcedump\_pov.cpp: Outputs pov files in a folder **pov\_files** by reading both the dump (bin or text file) and the force dump (txt) files.

Help on the input parameters for the programs above are given by running the program with *-h* or *--help* flags. 

The programs can be built by cmake (minimum version 2.8), following the steps below
1. mkdir build
2. cd build
3. cmake ..
4. make
5. make install

If cmake is not installed on your machine, use the **Makefile** to build the files. Since I am not very familiar with GNU make, you will have to copy all of the lib and program files into the same directory before using the **Makefile**.



