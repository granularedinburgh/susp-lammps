/*!
 * \brief PoVBox is derived from BoxProp class to output Povray files
 *
 * \author Ranga Radhakrishnan
 * \date 07/11/2019
 *
 *\class PoVBox is used to create an object to read in a dump file of a sheared box and output the deformed box, contacts as bonds and particles in povray format in a folder 
 */



#include"box_properties.h"
#include"definitions.h"
#include<map>
#ifndef _POVBOX_H
#define _POVBOX_H
class PoVBox: public BoxProp{

  public:
    //! Constructor which extends BoxProp with positions for atom type, radius position and x position
    PoVBox(int nVal,double dT,VEC_DBLE rate,int tpos=2,int xpos=5,int rpos=3)
      :BoxProp(nVal,dT,rate),type_pos(tpos),x_pos(xpos),rad_pos(rpos){}

    double getRad(int) const;//!< get radius of particle i
    int getType(int) const;//!< get type of particle i
    VEC_DBLE getPos(int) const;//!<  get position of particle i

    //!override BoxProp (parent) functions
    bool readBinData(std::ifstream& fp){
      bool out=BoxProp::readBinData(fp);  
      storeAtomID();
      createDir();
      return out;
    }

    //!override BoxProp (parent) functions
    bool readFormData(std::ifstream& fp){
      bool out=BoxProp::readFormData(fp);  
      storeAtomID();
      createDir();
      return out;
    }


    //!override BoxProp (parent) functions
    double getItem(int i,int lpos) const{
      return(BoxProp::getItem(getAtomID(i),lpos));
    }

    //!write periodic box in povray format
    bool writeBox(int) const;
    //!write particles and contacts in povray format based on a contact list 
    bool writeAtomsBonds(int count, ATOM_CONTACT_LIST const& myList) const
    {
      writeAtoms(count,myList.atomList);
      writeBonds(count,myList.contactList);
      return 0;
    }

    //!write particles in povray format based on an Atom list 
    bool writeAtoms(int,LIST_ATOM const&) const;
    //!write particles in povray format based on a Contact list 
    bool writeBonds(int,VEC_CONTACT const&) const;

    //!output contacts (with a default value of Kn=1.0 as scaling)
    ATOM_CONTACT_LIST output_list(double Kn=1.0) const;

    //!Some test functions to test the class
    void test() ;

  private:
    int type_pos,x_pos,rad_pos;
    std::map<int,int> atomID; //!< data structure maps atom ID (index) with respect to the output line the dump file
    void storeAtomID();//!< Function to map atom ID to dump file

    //!get atom ID (index) from a map (after its creation and storage)
    int getAtomID(int i) const{ return( atomID.at(i) ); }

    VEC_DBLE calcDim() const;

    //!outputdir flag
    bool outdir_flag=false;
    //!create output dir
    void createDir();

    //!preamble text
    STRING sph_pream="SPH(<";
    STRING sph_end=")";
    STRING cyl_pream="CYL(<";
    STRING cyl_end=")";
    STRING edge_pream="EDGE(<";
    STRING edge_end="1)";
    STRING dir_name="pov_files/";

};

#endif
