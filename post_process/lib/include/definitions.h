/*!
 * \brief Declarations that are useful for all classes and programs
 *
 *
 * \author Ranga Radhakrishnan
 * \date 07/11/2019
 *
 */

#include<vector>
#include<set>
#include<cstdint>
#include<string>

#ifndef _DEF_H
#define _DEF_H

/*!
 * A few size variables to get data from bin file
 */
typedef uint64_t BIGINT;
const unsigned long int sizeBig=sizeof(BIGINT);
const unsigned long int sizeInt=sizeof(int);
const unsigned long int sizeDouble=sizeof(double);

typedef std::vector<double>  VEC_DBLE; //!< A short name for a double vector
typedef std::vector<int>  VEC_INT;//!< A short name for a integer vector
typedef std::string STRING;
typedef std::set<uint16_t> LIST_ATOM; //!< Atom list is a saved as set object from STL

typedef std::pair<uint16_t,uint16_t> ATOM_PAIR; //!< A variable to store atom pair
typedef std::vector<ATOM_PAIR> VEC_ATOMPAIR;//!< Vector of atom pairs
typedef std::vector<bool> VEC_BOOL;//!< Vector of boolean
enum CTYPE{frictionless,frictional}; //!< Distinguish between different contact types

//! A datastructure prototype which contains a contact: two atoms, two forces, and the type of contact
typedef struct{
  uint16_t atom1,atom2;
  double Fn=0,Ft=0;
  CTYPE type;
}  CONTACT;

typedef std::vector<CONTACT> VEC_CONTACT;

//! A data structure definition for all contacts and atoms
typedef struct{
  VEC_CONTACT contactList;
  LIST_ATOM atomList;
} ATOM_CONTACT_LIST;

/*!
 * A template needed for binary I/O (from Stroustrup's book on Programming)
 */
template<class T> char* as_bytes(T& i)  
{
  void* addr = &i;      //get the address of the first byte of memory used to store the object
  return static_cast<char*>(addr); //treat that memory as bytes
}
#endif
