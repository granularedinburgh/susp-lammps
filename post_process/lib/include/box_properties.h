/*!
 * \brief BoxProp is derived from SimBox class to read sheared boxes 
 *
 * \author Ranga Radhakrishnan
 * \date 07/11/2019
 *
 *\class BoxProp is used to create an object to read in a dump file of a sheared box and store them into the data structure defined by its parent SimBox.
 */

#include"definitions.h"
#include"simbox.h"

#ifndef _BOXPROP_H
#define _BOXPROP_H


class BoxProp : public SimBox{
  public:
    //! Constructor which assigns no_values per line of dump data, value of time step and the shear rate double vector
    BoxProp(int nVal,double dT,VEC_DBLE rate)
      :SimBox(nVal),dt(dT),vec_gdot(rate){shear=true;}

    //! Default Constructor which assigns no_values per line of dump data, which sets shear rate double vector to be 0
    BoxProp(int nVal)
      :SimBox(nVal){dt=0;vec_gdot.assign(3,0.0);vec_delta.assign(3,0.0);shear=false;}

    bool readBinData(std::ifstream &); //!< Function over-ride simbox.h
    bool readFormData(std::ifstream &); //!< Function over-ride simbox.h

    protected:
    VEC_DBLE vec_delta;//!< Box deformation Vector of size 3 (Maximum allowed by LAMMPS) 

  private:
    double dt;//!< timestep value
    VEC_DBLE vec_gdot;//!< Shear rates in xy,yz,xz directions 
    void vecBoxDisp();//!< Function to calculate displacement
    bool shear;//!< Boolean to indicate shear deformation of the box


};

#endif
