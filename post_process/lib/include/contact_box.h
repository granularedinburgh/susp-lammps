/*!
 * \brief CntctBox is derived from BoxProp class to calculate the number of contacts
 *
 * \author Ranga Radhakrishnan
 * \date 07/11/2019
 *
 *\class CntctBox is used to create an object to read in a dump file of a sheared box and calculate the number of contacts
 */


#include"box_properties.h"

#ifndef _CNTCTBOX_H
#define _CNTCTBOX_H
class CntctBox: public BoxProp{

  public:
    //! Constructor which assigns no_values per line of dump data
    CntctBox(int nVal,double dT,VEC_DBLE rate,int tpos=2,int xpos=5,int rpos=3)
      :BoxProp(nVal,dT,rate),type_pos(tpos),x_pos(xpos),rad_pos(rpos){}

    //! get radius of particle i
    double getRad(int i) const 
    {return BoxProp::getItem(i,rad_pos);}

    //! get type of particle i
    int getType(int i) const;

    //! calculate contacts
    VEC_DBLE calc_Cntct() const;
    //! calculate contacts
    double calc_Cntct_exclude() const;
    //! Test function for the class
    void Test() const;

  private:
    int type_pos,x_pos,rad_pos;

    //! Function to get the position of element i 
    VEC_DBLE getPos(int) const;
};

#endif
