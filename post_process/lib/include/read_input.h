#include"definitions.h"
#ifndef _READINPUT_H
#define _READINPUT_H
bool read_term_cntct(int, char **,int&,int&,VEC_DBLE&,double&, STRING&);
bool read_term_pov(int, char **,int &,VEC_DBLE&,double&,STRING&,STRING&,CTYPE&);
bool read_force_cntct(int, char **,int&,STRING&);
#endif
