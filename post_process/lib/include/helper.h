#include "definitions.h"
#ifndef _HELPER_H
#define _HELPER_H
VEC_DBLE mean_dev(VEC_DBLE const &);
bool write_line(std::ofstream& ,VEC_DBLE,VEC_DBLE,STRING,STRING);
VEC_DBLE vec_sum(VEC_DBLE,VEC_DBLE);
VEC_DBLE ccdDist(VEC_DBLE const &,VEC_DBLE const & ,VEC_DBLE const &,VEC_DBLE const &);
VEC_DBLE ccdDist(VEC_DBLE const &,VEC_DBLE const & ,VEC_DBLE const &,VEC_DBLE const &,VEC_BOOL const &);
#endif

