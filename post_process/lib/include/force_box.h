/*!
 *
 *
 * Base class reads a force dump file which contains formatted data 
 * and outputs an atom and a contact list for use by other functions
 *
 * \author Ranga Radhakrishnan
 * \date 07/11/2019
 *
 */

#include"definitions.h"
#include<iostream>

#ifndef _FORCEBOX_H
#define _FORCEBOX_H

//! \brief A structure to store a contacting atom pair, type of contact and the values
struct FVal{
ATOM_PAIR atoms; 
int cntct_type; 
VEC_DBLE values;
};


//! \brief ForceBox creates an object that reads in a forcedump file 
class ForceBox{

  public:
    //! Default constructor has not read any file yet
    ForceBox()
     {read=false;}

    //! Function to read the force data from an input file
    bool readForceData(std::ifstream &);

    double getItem(int,int) const; //!< get item i, value j
    int getCtype(int) const; //!< get item i, contact type
    ATOM_PAIR getAtoms(int) const; //!< get item i, atom pair

    //! get number of values of the ith element 
    int getNvalues(int ii) const
    {return entries[ii].values.size();}

    //! get the total number of entries at a given time step
    int getNentries() const
    {return N_entries;}

    //! get the current timestep value
    int getTime() const
    {return timestep;}

    //! calculate and output contacts and atoms based on contact type
    bool getAtomsContacts(CTYPE,VEC_BOOL&,VEC_CONTACT&) const;
    //! Same caculations with a different output data structure
    ATOM_CONTACT_LIST getAtomsContacts(CTYPE type) const;

    //!Some test functions to test the class
    void test(int) const;
    
  private:
  int timestep=0,N_entries=0;
  
  bool read=false;
  std::vector<FVal> entries;
};

#endif
