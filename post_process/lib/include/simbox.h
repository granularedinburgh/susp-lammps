/*!
 * \brief Base class that could read in a dump file 
 *
 * Base class that could read in a dump file which contains binary or formatted data 
 * It reads in one time step at a time
 *
 * \author Ranga Radhakrishnan
 * \date 07/11/2019
 *
 */
#include"definitions.h"

#ifndef _SIMBOX_H
#define _SIMBOX_H

/* \class Simbox is used to create an object to read in a dump file and store into double vectors. It provides some functions for data access.*/
class SimBox{

  public:
    //! Constructor which assigns no_values per line of dump data
    SimBox(int nVal)
      :no_values(nVal){}
    bool readBinData(std::ifstream &); //!< Function to read in a binary file
    bool readFormData(std::ifstream &);//!< Function to read in Formatted data
    double getItem(int,int) const; //!< get item in Line i, value j
    BIGINT getNatoms() const  //! Function to output number of atoms in a timestep
    {return natoms;}
    BIGINT getTime() const //! Function to output the timestep
    {return timestep;}

  protected:
    VEC_DBLE boxl; //!< Variable to store box sizes
    //! variables that have box dimensions at a current time step
    double xbox[3],ybox[3],zbox[3]; 
    int boundary[3][2];//!< boundary variable outputted by LAMMPS is stored

  private:
    int no_values; //!< Number of values per line
    BIGINT timestep=0,natoms=0;//!< Current timestep, and total number of atoms. BIGINT is used because LAMMPS save these variables in this format

    VEC_DBLE values; //!< All the values (for all atoms are saved here)

    void boxDim();//!< Box dimensions are calculated by this private function and saved in boxl

};

int periodic_char(char); //!< periodic char
std::vector<std::string> split_words(std::string); //!< Split a long string into words

#endif
