#include<fstream>
#include<cmath>
#include"force_box.h"
#include<iostream>
#include<sstream>

//Function to parse the dat in
bool ForceBox::readForceData(std::ifstream &myfile){


  if(myfile.eof()) return true;

  std::string line;
  std::istringstream iss;

  std::getline(myfile,line);
  //line 2 has timestep
  std::getline(myfile,line);
  iss.str(line);
  iss>>timestep;
  iss.clear();

  std::getline(myfile,line);
  //line 4 has no. of entries
  std::getline(myfile,line);
  iss.str(line);
  iss>>N_entries;

  //std::cout<<N_entries<<std::endl;

  entries.resize(N_entries);


  for(int ii=0;ii<5;ii++)std::getline(myfile,line);

  for(int ii=0;ii<N_entries;ii++){

    FVal one_entry;
    std::getline(myfile,line);
    std::istringstream input(line);

    input>>one_entry.atoms.first>>one_entry.atoms.second;
    input>>one_entry.cntct_type;


    double val;
    while(input>>val){
      one_entry.values.push_back(val);
    }

    entries[ii]=one_entry;

  }

  read=true;


  return false;

}


double ForceBox::getItem(int i,int lpos) const{
  return entries[i].values[lpos];
}

int ForceBox::getCtype(int i) const{
  return entries[i].cntct_type;
}


ATOM_PAIR ForceBox::getAtoms(int i) const{ 
  return entries[i].atoms;
}

ATOM_CONTACT_LIST ForceBox::getAtomsContacts(CTYPE type) const{

  ATOM_CONTACT_LIST aList;

  for(int ii=0;ii<N_entries;ii++){
    if(getCtype(ii)!=0)
    { 
      CONTACT c1;
      c1.Ft=sqrt( pow(getItem(ii,0),2)+pow(getItem(ii,1),2)+pow(getItem(ii,2),2) ); 
      c1.Fn=sqrt( pow(getItem(ii,3),2)+pow(getItem(ii,4),2)+pow(getItem(ii,5),2) ); 
      // Here, I used Ft >0 to take all values of Ft as frictional. Perhaps, it is wiser to choose a minimum value in some cases.
      // depending on input type
      if( (type==frictional && c1.Ft > 0 ) || type==frictionless)
      {
        ATOM_PAIR atoms=getAtoms(ii); 

        c1.atom1=atoms.first-1;
        c1.atom2=atoms.second-1;
        c1.type=type;
        aList.contactList.push_back(c1); 

        aList.atomList.insert(c1.atom1);
        aList.atomList.insert(c1.atom2);

      }
    }
  }
  //aList.atomList.unique();

  return aList;
}





bool ForceBox::getAtomsContacts(CTYPE type,VEC_BOOL& aList,VEC_CONTACT& contactList) const{

  //modify elements in range using Proxy iterators!
  for(auto&& x: aList) x=false;

  for(int ii=0;ii<N_entries;ii++){
    if(getCtype(ii)!=0)
    { 
      CONTACT c1;
      c1.Ft=sqrt( pow(getItem(ii,0),2)+pow(getItem(ii,1),2)+pow(getItem(ii,2),2) ); 
      c1.Fn=sqrt( pow(getItem(ii,3),2)+pow(getItem(ii,4),2)+pow(getItem(ii,5),2) ); 
      if( (type==frictional && c1.Ft > 1e-15 ) || type==frictionless)
      {
        ATOM_PAIR atoms=getAtoms(ii); 

        c1.atom1=atoms.first-1;
        c1.atom2=atoms.second-1;
        c1.type=frictional;
        contactList.push_back(c1); 

        aList[c1.atom1]=true;
        aList[c1.atom2]=true;
      }
    }
  }

  return 0;
}


//Testing the readfile
void ForceBox::test(int input) const{


  if(read==false) {
    std::cerr<<"Nothing read"<<std::endl;  
    return;
  }


  std::ofstream oFile;

  oFile.open("testForces.out",std::ofstream::out);

  if(oFile.fail()){
    std::cerr<<"Couldn't open the file!"<<std::endl;
    return;
  }

  //VEC_BOOL aList;
  //VEC_CONTACT contactList;

  //CTYPE contact_type=frictionless;
  //getContacts(3744,aList,contactList,contact_type);

  //int count=0;
  for(int ii=0;ii<N_entries;ii++){
    ATOM_PAIR a1=getAtoms(ii);
    oFile<<a1.first<<" "<<a1.second<<" ";
    int  ctype=getCtype(ii);
    oFile<<ctype<<" ";
    /* 
       if(ctype!=0)
       {
       oFile<<"a1\t"<<a1.first<<" "<<a1.second<<" ";
       oFile<<"a2\t"<<contactList[count].atom1<<" "<<contactList[count].atom2<<std::endl;
       count++;
       }
       */

    int nvals=getNvalues(ii);

    for(int jj=0;jj<nvals;jj++){
      double val=getItem(ii,jj);
      oFile<<val<<" ";
    }
    oFile<<"\n";

  }
  oFile<<"finish"<<input<<std::endl;
  oFile.close();

  return;
}
