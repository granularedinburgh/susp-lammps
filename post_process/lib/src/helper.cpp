#include"helper.h"
#include<algorithm>
#include<iostream>
#include<fstream>

//Calculate mean and standard deviation
VEC_DBLE mean_dev(VEC_DBLE const &arr){
  //calculate average arraration
  double mean_arr=accumulate(arr.begin(),arr.end(),0.0)/(1.0*arr.size());
  //calculate std. dev. arraration
  VEC_DBLE diff(arr.size());
  transform(arr.begin(), arr.end(), diff.begin(), [mean_arr](double x) { return x - mean_arr; });
  double std_arr=sqrt(inner_product(diff.begin(),diff.end(),diff.begin(),0.0)/(1.0*arr.size()-1.0));
  VEC_DBLE result;

  result.push_back(mean_arr);
  result.push_back(std_arr);

  return result;
}

/*
//minimum image convention for positions
VEC_DBLE ccdDist(VEC_DBLE x,VEC_DBLE y,VEC_DBLE l,double delx) {
VEC_DBLE r(3);
r[0]=y[0]-x[0]; 
r[1]=y[1]-x[1];
r[2]=y[2]-x[2];

help_LEBC(r,l,delx);
r[3] = sqrt(r[0]*r[0]+ r[1]*r[1]+ r[2]*r[2]);
return(r);
}

void help_LEBC(VEC_DBLE& r, VEC_DBLE l,double delx) {
  double cory = round(r[1] / l[1]); // assume shear rate = dvx/dy
  r[0] = r[0] - cory * delx;
  r[0] = r[0] - round(r[0]/l[0]) * l[0];
  r[1] = r[1] - cory * l[1];
  r[2] = r[2] - round(r[2]/l[2]) * l[2];
}
*/

//minimum image convention for positions
VEC_DBLE ccdDist(VEC_DBLE const& x,VEC_DBLE const& y,VEC_DBLE const& l,VEC_DBLE const& del) {

  VEC_DBLE r(4);
  r[0]=y[0]-x[0]; 
  r[1]=y[1]-x[1];
  r[2]=y[2]-x[2];

  double cory = round(r[1] / l[1]); 
  double corz = round(r[2] / l[2]); 

  r[0] = r[0] - cory * del[0];//del[0]=xy
  r[0] = r[0] - corz * del[1];//del[1]=xz
  r[1] = r[1] - corz * del[2];//del[2]=yz

  double corx = round(r[0] / l[0]);
  cory = round(r[1] / l[1]); 
  //no change to z direction

  r[0] = r[0] - corx * l[0];
  r[1] = r[1] - cory * l[1];
  r[2] = r[2] - corz * l[2];


  r[3] = sqrt(r[0]*r[0]+ r[1]*r[1]+ r[2]*r[2]);

  return(r);
}

//minimum image convention for positions
VEC_DBLE ccdDist(VEC_DBLE const& x,VEC_DBLE const& y,VEC_DBLE const& l,VEC_DBLE const& del,VEC_BOOL const& periodic) {

  VEC_DBLE r(4);
  r[0]=y[0]-x[0]; 
  r[1]=y[1]-x[1];
  r[2]=y[2]-x[2];
 
  double cory=0,corz=0,corx=0;
  if(periodic[1]) cory = round(r[1] / l[1]); 
  if(periodic[2]) corz = round(r[2] / l[2]); 

  r[0] = r[0] - cory * del[0];//del[0]=xy
  r[0] = r[0] - corz * del[1];//del[1]=xz
  r[1] = r[1] - corz * del[2];//del[2]=yz

  if(periodic[0]) corx = round(r[0] / l[0]);

  if(periodic[1]) cory = round(r[1] / l[1]); 
  //no change to z direction

  r[0] = r[0] - corx * l[0];
  r[1] = r[1] - cory * l[1];
  r[2] = r[2] - corz * l[2];

  r[3] = sqrt(r[0]*r[0]+ r[1]*r[1]+ r[2]*r[2]);

  return(r);
}

//write line function
bool write_line(std::ofstream& myfile,VEC_DBLE p1,VEC_DBLE p2,STRING preamble,STRING ender)
{
  if(!myfile.is_open()){
    std::cerr<<"File is not open at function helper.h::write_line()";
    exit(1);
  }

  myfile<<preamble<<p1[0]<<","<<p1[1]<<","<<p1[2]<<">,";
  myfile<<"<"<<p2[0]<<","<<p2[1]<<","<<p2[2]<<">,"<<ender<<std::endl;
  return 0;
}

//sum of two vectors
VEC_DBLE vec_sum(VEC_DBLE x,VEC_DBLE y){
  if(x.size() != y.size()){
    std::cerr<<"dimensions of the vectors in vec_sum don't match";
    exit(1);
  }
  VEC_DBLE result=x;
  for(int i=0; i != x.size(); i++)
    result[i]=x[i]+y[i];
  return result;
}
