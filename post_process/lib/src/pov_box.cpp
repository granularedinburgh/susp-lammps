#include"pov_box.h"
#include"helper.h"
#include<iostream>
#include<iomanip>
#include<cmath>
#include<fstream>
#include <sstream>

//utility function to set file names
STRING setFname(STRING dir_name,STRING partName,int count)
{
  //set file name
  std::stringstream ss;
  ss << std::setw(5) << std::setfill('0') << count;
  STRING fName=dir_name+partName+"_"+ss.str()+".pov";
  return(fName);
}


//Get a vector of position
VEC_DBLE PoVBox::getPos(int i) const{
  VEC_DBLE res;
  for(int aa=0;aa<3;aa++)
    res.push_back(getItem(i,x_pos+aa));
  return res;
}


//Store an associative array of atom positions
void PoVBox::storeAtomID(){
  int Natoms=getNatoms();
  //atomID.resize(Natoms,0);
  atomID.clear();
  for(int i=0;i<Natoms;i++)
  {
    int id=static_cast<int>(std::round(BoxProp::getItem(i,1)));
    atomID[id-1]=i;
  }
}

//get type of atom i
int PoVBox::getType(int i) const {
  int val=std::round(getItem(i,type_pos));  
  return static_cast<int>(val);
} 

//get radius of atom i
double PoVBox::getRad(int i) const 
{return getItem(i,rad_pos);}

//write the boundaries
bool PoVBox::writeBox(int count) const{
  //set file name
  STRING box_fname=setFname(dir_name,"box",count);

  //box position
  std::ofstream box_file;
  box_file.open(box_fname);

  if(box_file.eof()) return(1);

  double xy=xbox[2];
  double xz=ybox[2];
  double yz=zbox[2];


  VEC_DBLE a(3),b(3),c(3),base(3);

  //refer to triclinic box discussion in lammps
  a[0]=boxl[0]; a[1]=0;  a[2]=0; 
  b[1]=boxl[1]; b[0]=xy; b[2]=0;
  c[2]=boxl[2]; c[0]=xz; c[1]=yz;


  base[0]=boxl[3]; base[1]=boxl[4]; base[2]=boxl[5];  //xlo,ylo,zlo or the origin of the box

  //parallelogram rule
  VEC_DBLE c1=vec_sum(a,b), c2=vec_sum(a,c), c3=vec_sum(b,c), c4=vec_sum(c1,c); 

  //lower rectangle
  write_line(box_file,base,vec_sum(base,a),edge_pream,edge_end); //(base,base+a)
  write_line(box_file,base,vec_sum(base,b),edge_pream,edge_end); //(base,base+b)
  write_line(box_file,vec_sum(base,a),vec_sum(base,c1),edge_pream,edge_end); //(base+a,base+a+b)
  write_line(box_file,vec_sum(base,b),vec_sum(base,c1),edge_pream,edge_end); //(base+b,base+a+b)

  //upper rectangle
  write_line(box_file,vec_sum(base,c),vec_sum(base,c2),edge_pream,edge_end); //(base+c,base+a+c)
  write_line(box_file,vec_sum(base,c),vec_sum(base,c3),edge_pream,edge_end); //(base+c,base+b+c)
  write_line(box_file,vec_sum(base,c2),vec_sum(base,c4),edge_pream,edge_end); //(base+a+c,base+a+b+c)
  write_line(box_file,vec_sum(base,c3),vec_sum(base,c4),edge_pream,edge_end); //(base+b+c,base+a+b+c)


  //front edges (2)
  write_line(box_file,base,vec_sum(base,c),edge_pream,edge_end); //(base,base+c)
  write_line(box_file,vec_sum(base,a),vec_sum(base,c2),edge_pream,edge_end); //(base+a,base+a+c)

  //back edges (2)
  write_line(box_file,vec_sum(base,b),vec_sum(base,c3),edge_pream,edge_end); //(base+b,base+b+c)
  write_line(box_file,vec_sum(base,c1),vec_sum(base,c4),edge_pream,edge_end); //(base+a+b,base+a+b+c)



  box_file.close();

  return 0;
}

bool PoVBox::writeAtoms(int count,LIST_ATOM const& aList) const
{
  //set file name
  STRING file_sphere=setFname(dir_name,"particles",count);
  //xyz positions
  std::ofstream fsph;
  fsph.open(file_sphere);
  VEC_DBLE xPos;

  int Natoms=getNatoms();
  std::vector<bool> restAtoms(Natoms);
  /*
  if(aList.size() != Natoms){
    std::cerr<<"Atom list not constructed properly in ForceBox class. Please reconstruct!";
    exit(1);
  }
  */
  //output all contacting atoms
  for(auto const& atomID:aList){
      restAtoms[atomID]=true;
      xPos=getPos(atomID);
      fsph<<sph_pream<<xPos[0]<<","<<xPos[1]<<","<<xPos[2]<<">,"<<getRad(atomID)<<sph_end<<std::endl;
    }
  fsph.close();

  //output all non-contacting atoms
  STRING all_sphere=setFname(dir_name,"rest_particles",count);
  //xyz positions
  std::ofstream fatoms;
  fatoms.open(all_sphere);
  for(int i=0;i<Natoms;i++){
    if(!restAtoms[i])
    {
      xPos=getPos(i);
      fatoms<<sph_pream<<xPos[0]<<","<<xPos[1]<<","<<xPos[2]<<">,"<<getRad(i)<<sph_end<<std::endl;
    }
    }
  fatoms.close();



  return 0;
}



bool PoVBox::writeBonds(int count,VEC_CONTACT const& bonds) const
{
  //set file name
  STRING file_cyl=setFname(dir_name,"bonds",count);
  std::ofstream fcyl;
  fcyl.open(file_cyl);


  int num_bonds=bonds.size();

  for(int i=0;i<num_bonds;i++){
    int atom1=bonds[i].atom1;
    int atom2=bonds[i].atom2;
    VEC_DBLE xPos=getPos(atom1);
    VEC_DBLE yPos=getPos(atom2);

    //double radsum=getRad(atom1)+getRad(atom2);

    //calculate min distance
    VEC_DBLE  ccd=ccdDist(xPos,yPos,boxl,vec_delta);
    VEC_DBLE  bond1,bond2;
    for(int jj=0;jj<3;jj++){
      bond1.push_back(xPos[jj]+ccd[jj]);
      bond2.push_back(yPos[jj]-ccd[jj]);
    }


    fcyl<<cyl_pream<<xPos[0]<<","<<xPos[1]<<","<<xPos[2]<<">,";
    fcyl<<"<"<<bond1[0]<<","<<bond1[1]<<","<<bond1[2]<<">,"<<bonds[i].Fn<<cyl_end<<std::endl;

    fcyl<<cyl_pream<<yPos[0]<<","<<yPos[1]<<","<<yPos[2]<<">,";
    fcyl<<"<"<<bond2[0]<<","<<bond2[1]<<","<<bond2[2]<<">,"<<bonds[i].Fn<<cyl_end<<std::endl;

  }
  fcyl.close();

  return 0;
}


//Test functions
void PoVBox::test() 
{
  int Natoms=getNatoms();

  std::ofstream oFile;
  oFile.open("testDump.out",std::ofstream::out);

  if(oFile.fail()){
    std::cerr<<"Couldn't open the file!"<<std::endl;
    return;
  }



  for(int ii=0;ii<Natoms;ii++)
  {
    oFile<<(int)getItem(ii,1)<<"\t";
    oFile<<(int)getType(ii)<<"\t "<<getRad(ii);
    VEC_DBLE pos=getPos(ii);
    oFile<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<std::endl;
  }
  oFile.close();

  /*
   // old code where I used input type to determine frictional or frictionless contacts
     for(int i=0;i<Natoms-1;i++) {
     double r1=getRad(i);
     USint type1=getType(i);
     std::cout<<"i="<<i<<std::endl;
     for(int j=i+1;j<Natoms;j++) {
     USint tot_type=type1+getType(j);
     double radsum=r1+getRad(j);

     VEC_DBLE ccd=ccdDist(i,j);
     double hsep=radsum-ccd[3];

     if(hsep>0)
     std::cout<<"\t j="<<j<<"\t hsep = "<<hsep<<std::endl;
     } } 
     */

}

void PoVBox::createDir()
{
  if(outdir_flag==0)
  {
    //create new directory
    const int dir_err = system("mkdir -p pov_files");
    if (-1 == dir_err)
    {
      printf("Error creating directory!n");
      exit(1);
    }
    outdir_flag=1;
  }
}


//output contact and bool atom list
ATOM_CONTACT_LIST PoVBox::output_list(double Kn) const
{

  int Natoms=getNatoms();


  VEC_BOOL periodicity{0,0,0};
  for(int i=0;i<3;i++)
  {
    if( boundary[i][0]==0 && boundary[i][1]==0 ) periodicity[i]=1;
  }

  bool periodic=(periodicity[0] & periodicity [1] & periodicity [2]);

  ATOM_CONTACT_LIST aList;
  for(auto it1=atomID.begin();it1 != std::prev(atomID.end());++it1)
  {
    int i=it1->first;
    double r1=getRad(i);
    VEC_DBLE xPos=getPos(i);
    for(auto it2=std::next(it1);it2 != atomID.end();++it2)
    {
      int j=it2->first;
      double radsum=r1+getRad(j);
      VEC_DBLE yPos=getPos(j);

      VEC_DBLE ccd;
      if(periodic)
        ccd=ccdDist(xPos,yPos,boxl,vec_delta);
      else
        ccd=ccdDist(xPos,yPos,boxl,vec_delta,periodicity);

      double hsep=radsum-ccd[3];

      if(hsep>0){
        //sep.push_back(hsep);
        CONTACT c1;
        c1.atom1=i; c1.atom2=j;
        c1.type=frictionless; //assign the type as frictionless by default
        c1.Fn=hsep*Kn; //output based on Kn
        c1.Ft=0;//assign tangential force to be 0
        aList.contactList.push_back(c1);

        aList.atomList.insert(c1.atom1);
        aList.atomList.insert(c1.atom2);
      }
    }
  }
  return aList;
}
