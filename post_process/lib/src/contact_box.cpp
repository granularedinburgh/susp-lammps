#include"helper.h"
#include"contact_box.h"
#include<iostream>
#include<cmath>
#include<numeric>
#include<fstream>


//Get a vector of position
VEC_DBLE CntctBox::getPos(int i) const{
  VEC_DBLE res;
  for(int aa=0;aa<3;aa++)
    res.push_back(getItem(i,x_pos+aa));

  return res;
}

//Get a vector containing the number of contacts, and average seperation
//big assumption involving two particle types being small (type 1) and big particles (type 2) respectively 
//This can be generalised in the future
VEC_DBLE CntctBox::calc_Cntct() const
{
  BIGINT bscnt=0,bbcnt=0,sscnt=0;

  double tot_sep=0.0;
  int num_cntcts=0;

  int Natoms=getNatoms();

  VEC_BOOL periodicity{0,0,0};
  for(int i=0;i<3;i++)
  {
    if( boundary[i][0]==0 && boundary[i][1]==0 ) periodicity[i]=1;
  }

  bool periodic=(periodicity[0] & periodicity [1] & periodicity [2]);


  for(int i=0;i<Natoms-1;i++)
  {
    double r1=getRad(i);
    int type1=getType(i);
    VEC_DBLE xPos=getPos(i);
    for(int j=i+1;j<Natoms;j++)
    { 
      
      //type is dependent on contact type 
      int tot_type=type1+getType(j);
      double radsum=r1+getRad(j);

      VEC_DBLE yPos=getPos(j);
      VEC_DBLE ccd;
      if(periodic)
        ccd=ccdDist(xPos,yPos,boxl,vec_delta);
      else
        ccd=ccdDist(xPos,yPos,boxl,vec_delta,periodicity);


      double hsep=radsum-ccd[3];

      if(hsep>0){
        //sep.push_back(hsep);
        tot_sep+=hsep;
        num_cntcts++;
        switch (tot_type){
          case 2: sscnt+=2; break;
          case 3: bscnt+=1; break;
          case 4: bbcnt+=2; break;
        }
      }
    }
  }

  double bb_num=(bbcnt+bscnt)*1.0;
  double ss_num=(sscnt+bscnt)*1.0;
  double tot_num=(sscnt+2*bscnt+bbcnt)*1.0;
  double ave_sep=tot_sep/(num_cntcts*1.0);

  VEC_DBLE result={ss_num,bb_num,tot_num,ave_sep};
  return result;
}

//Get a vector containing the number of contacts excluding rattlers
double CntctBox::calc_Cntct_exclude() const
{
  int Natoms=getNatoms();
  VEC_INT perAtom;
  perAtom.assign(Natoms,0);

  VEC_BOOL periodicity{0,0,0};
  for(int i=0;i<3;i++)
  {
    if( boundary[i][0]==0 && boundary[i][1]==0 ) periodicity[i]=1;
  }

  bool periodic=(periodicity[0] & periodicity [1] & periodicity [2]);

  for(int i=0;i<Natoms-1;i++)
  {
    double r1=getRad(i);
    VEC_DBLE xPos=getPos(i);
    for(int j=i+1;j<Natoms;j++)
    {
      double radsum=r1+getRad(j);

      VEC_DBLE yPos=getPos(j);

      VEC_DBLE ccd;
      if(periodic)
        ccd=ccdDist(xPos,yPos,boxl,vec_delta);
      else
        ccd=ccdDist(xPos,yPos,boxl,vec_delta,periodicity);

      double hsep=radsum-ccd[3];

      if(hsep>0){
        perAtom[i]+=1;
        perAtom[j]+=1;
      }
    }

  }

  BIGINT totcnt=0; 
  int included=0;
  for(int i=0;i<Natoms-1;i++)
  {
    if(perAtom[i]>=1){
      ++included;
      totcnt+=perAtom[i];
    }
  }


  double result=(included!=0)?(totcnt*1.0)/(included*1.0):0;

  return result;

}




int CntctBox::getType(int i) const{
  int val=std::round(getItem(i,type_pos));  
  return static_cast<int>(val);
} 


//Test functions
void CntctBox::Test() const
{
  int Natoms=getNatoms();

  std::ofstream oFile;
  oFile.open("test.out",std::ofstream::out);

  if(oFile.fail()){
    std::cerr<<"Couldn't open the file!"<<std::endl;
    return;
  }

  for(int ii=0;ii<Natoms;ii++)
  {
    oFile<<(int)getType(ii)<<"\t "<<getRad(ii);
    VEC_DBLE pos=getPos(ii);
    oFile<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<std::endl;
  }

  oFile.close();

  /*
     for(int i=0;i<Natoms-1;i++) {
     double r1=getRad(i);
     int type1=getType(i);
     std::cout<<"i="<<i<<std::endl;
     for(int j=i+1;j<Natoms;j++) {
     int tot_type=type1+getType(j);
     double radsum=r1+getRad(j);

     VEC_DBLE ccd=ccdDist(i,j);
     double hsep=radsum-ccd[3];

     if(hsep>0)
     std::cout<<"\t j="<<j<<"\t hsep = "<<hsep<<std::endl;
     } } 
     */

}
