#include"read_input.h"
#include<iostream>
#include<string>
#include<getopt.h>
#include<cmath>

void print_help_pov()
{
  std::cout <<
    "--gdot_xy <g>        : Strain rate value in the simulation in xy direction. Default: 0\n"
    "--gdot_xz           : Strain rate value in the xz direction. Default: 0\n"
    "--gdot_yz           : Strain rate value in the yz direction. Default: 0\n"
    "--delta_t <t>        : Time interval value in the simulation. Default: 10^-4\n"
    "--dump_file <d>      : Name of the input position dump file. Default: 'dump.bin' \n"
    "--nvalues <n>        : No. of values per step in dump file. Default: 19\n"
    "--force_file <f>     : Name of the input force dump file. Default: forces.lammpstrj \n"
    "--frictional <x>     : Flag for frictional contacts and atoms. If this flag is not included, output contains all contacting atoms. Default: frictionless \n"
    "--help <h>           : Show help\n";
  exit(1);
}

bool read_term_pov(int argc, char **argv,int& no_values,VEC_DBLE& vec_gdot,double& dt,STRING& dfile,STRING& ffile,CTYPE& type)
{
  //parse input file for gdot and timestep
  opterr=0;
#define MYOPT1 1000
#define MYOPT2 2000

  while (true)
  {
    static struct option long_options[] =
    {
      /* These options don’t set a flag.
         We distinguish them by their indices. */
      {"gdot_xy",required_argument, 0, 'g'},
      {"gdot_xz",required_argument, 0, MYOPT1},
      {"gdot_yz",required_argument, 0, MYOPT2},
      {"delta_t", required_argument, 0, 't'},
      {"nvalues", required_argument, 0, 'n'},
      {"dump_file", required_argument, 0, 'd'},
      {"force_file", required_argument, 0, 'f'},
      {"help", no_argument, 0, 'h'},
      {"frictional", no_argument, 0, 'x'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    int c=getopt_long(argc,argv,"g:t:n:d:f:hx",long_options,&option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    int dir=0;

    switch (c)
    {
      case 'g':
        vec_gdot[0] = atof(optarg);
        break;
      case MYOPT1:
        vec_gdot[1] = atof(optarg);
        break;
      case MYOPT2:
        vec_gdot[2] = atof(optarg);
        break;
      case 't':
        dt= atof(optarg);
        break;
      case 'n':
        no_values= atof(optarg);
        break;
      case 'd':
        dfile= STRING(optarg);
        break;
      case 'f':
        ffile= STRING(optarg);
        break;
      case 'x':
        type=frictional;
        break;
      case 'h':
        print_help_pov();
        return 1;
      case '?':
        if (optopt == 'g' || optopt == 't' || optopt == 'n' || optopt == 'd' || optopt == 'f'|| optopt == MYOPT1 || optopt == MYOPT2 )
          std::cerr<<"Option -"<<std::to_string(optopt)<<"requires an argument.\n";
        else if (isprint (optopt))
          std::cerr<< "Unknown option `-"<<std::to_string(optopt)<<"'"<<std::endl;
        else
          std::cerr<< "Unknown option character ` "<<std::to_string(optopt)<<"'"<<std::endl;
      default:
        print_help_pov();
        abort ();
    }

  }
  STRING phrase;
  (type==frictional)?phrase="frictional":phrase="frictionless";
  std::cout<<"Outputting "<<phrase<<" Contacts"<<std::endl;
  std::cout<<"Strain rate(xy) ="<<vec_gdot[0]<<"\t rate(xz) ="<<vec_gdot[1]<<"\t rate(yz) ="<<vec_gdot[2]<<"\n";
  std::cout<<"timestep size="<<dt<<"\t no_values/step in dump="<<no_values<<std::endl;
  std::cout<<"dump file="<<dfile<<"\t Force file="<<ffile<<std::endl;

  return 0;
}

void print_help_cntct()
{
  std::cout <<
    "--gdot_xy <g>        : Strain rate value in the simulation in xy direction. Default: 0\n"
    "--gdot_xz           : Strain rate value in the xz direction. Default: 0\n"
    "--gdot_yz           : Strain rate value in the yz direction. Default: 0\n"
    "--delta_t <t>   :       Time interval value in the simulation\n"
    "--dump_file <d> :       Name of the input dump file\n"
    "--nvalues <n>   :       No. of values per step in dump file\n"
    "--no_big <b>     :       No. of big particles in the simulation box\n"
    "--help <h>      :       Show help\n";
  exit(1);
}

bool read_term_cntct(int argc, char **argv,int& noBig,int& no_values,VEC_DBLE& vec_gdot,double& dt, STRING& dfile)
{
  //parse input file for gdot and timestep
  opterr=0;

#define MYOPT1 1000
#define MYOPT2 2000

  while (true)
  {
    static struct option long_options[] =
    {
      /* These options don’t set a flag.
         We distinguish them by their indices. */
      {"gdot_xy",required_argument, 0, 'g'},
      {"gdot_xz",required_argument, 0, MYOPT1},
      {"gdot_yz",required_argument, 0, MYOPT2},
      {"delta_t", required_argument, 0, 't'},
      {"nvalues", required_argument, 0, 'n'},
      {"dump_file", required_argument, 0, 'd'},
      {"no_big", required_argument, 0, 'b'},
      {"help", no_argument, 0, 'h'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    int c=getopt_long(argc,argv,"g:t:n:d:b:h",long_options,&option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    int dir=0;

    switch (c)
    {
      case 'g':
        vec_gdot[0] = atof(optarg);
        break;
      case MYOPT1:
        vec_gdot[1] = atof(optarg);
        break;
      case MYOPT2:
        vec_gdot[2] = atof(optarg);
        break;
      case 't':
        dt= atof(optarg);
        break;
      case 'n':
        no_values= atof(optarg);
        break;
      case 'd':
        dfile= STRING(optarg);
        break;
      case 'b':
        noBig=atof(optarg);
        break;
      case 'h':
        print_help_cntct();
        return 1;
      case '?':
        if (optopt == 'g' || optopt == 't' || optopt == 'n' || optopt == 'd' || optopt == 'b' || optopt == MYOPT1 || optopt == MYOPT2)
          std::cerr<<"Option -"<<std::to_string(optopt)<<"requires an argument.\n";
        else if (isprint (optopt))
          std::cerr<< "Unknown option `-"<<std::to_string(optopt)<<"'"<<std::endl;
        else
          std::cerr<< "Unknown option character ` "<<std::to_string(optopt)<<"'"<<std::endl;
      default:
        print_help_cntct();
        abort ();
    }
  }
  STRING phrase;
  std::cout<<"Calculating contact numbers"<<std::endl;
  std::cout<<"Strain rate(xy) ="<<vec_gdot[0]<<"\t rate(xz) ="<<vec_gdot[1]<<"\t rate(yz) ="<<vec_gdot[2]<<"\n";
  std::cout<<"timestep size="<<dt<<"\t no_values/step in dump="<<no_values<<std::endl;
  std::cout<<"dump file="<<dfile<<"\t No. of big particles="<<noBig<<std::endl;

  return 0;
}

void force_help_cntct()
{
  std::cout <<
    "--no_atoms <a>  :       Total number of atoms in the simulation box\n"
    "--force_file <f>:       Name of the input force dump file\n"
    "--help <h>      :       Show help\n";
  exit(1);
}

bool read_force_cntct(int argc, char **argv,int& nAtoms, STRING& ffile)
{
  opterr=0;

  while (true)
  {
    static struct option long_options[] =
    {
      /* These options don’t set a flag.
         We distinguish them by their indices. */
      {"no_atoms", required_argument, 0, 'a'},
      {"force_file", required_argument, 0, 'f'},
      {"help", no_argument, 0, 'h'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    int c=getopt_long(argc,argv,"a:f:h",long_options,&option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c)
    {
      case 'a':
        nAtoms= atoi(optarg);
        break;
      case 'f':
        ffile= STRING(optarg);
        break;
      case 'h':
        force_help_cntct();
        return 1;
      case '?':
        if (optopt == 'a' || optopt == 'f')
          std::cerr<<"Option -"<<std::to_string(optopt)<<"requires an argument.\n";
        else if (isprint (optopt))
          std::cerr<< "Unknown option `-"<<std::to_string(optopt)<<"'"<<std::endl;
        else
          std::cerr<< "Unknown option character ` "<<std::to_string(optopt)<<"'"<<std::endl;
      default:
        force_help_cntct();
        abort ();
    }
  }
  STRING phrase;
  std::cout<<"Calculating contact numbers"<<std::endl;
  std::cout<<"force dump file="<<ffile<<"\t No. of atoms="<<nAtoms<<std::endl;

  return 0;
}
