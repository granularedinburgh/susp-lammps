#include"simbox.h"
#include<fstream>
#include<iostream>
#include<algorithm>
#include<sstream>
#include<iterator>

//Function to parse the dat in
bool SimBox::readFormData(std::ifstream &myfile){

  int triclinic=1;
  if(myfile.eof()) return true;

  std::string line,lin2;
  std::istringstream iss;

  std::getline(myfile,line);
  //line 2 has timestep
  std::getline(myfile,line);
  iss.str(line);
  iss>>timestep;
  iss.clear(); //this clearing is quite important

  std::getline(myfile,line);
  //line 4 has no. of atoms
  std::getline(myfile,line);
  iss.str(line);
  iss>>natoms;
  iss.clear();

  std::getline(myfile,line);//line 5 has boundary info
  //split input into a vector vector
  std::vector<std::string> words=split_words(line);

  //6 is not triclinic, 9 is
  (words.size()==9)?triclinic=1:triclinic=0;
  //last three words contains boundary info
  for(int j=0;j<3;j++){
    int val=words.size()-3+j;
    boundary[j][0]=periodic_char(words[val][0]);
    boundary[j][1]=periodic_char(words[val][1]);
  }

  //Line 6--9 has box dimesions --- something wrong here!! 
  // std::getline(myfile,lin2); iss.str(lin2);
  // std::cout<<lin2;
  double val;
  myfile>>val;xbox[0]=val;
  myfile>>val;xbox[1]=val;
  if(triclinic)myfile>>xbox[2];

  myfile>>val;ybox[0]=val;
  myfile>>val;ybox[1]=val;
  if(triclinic)myfile>>ybox[2];

  myfile>>val;zbox[0]=val;
  myfile>>val;zbox[1]=val;
  if(triclinic)myfile>>zbox[2];

  /*
     std::cout<<"xbox"<<"\t"<<xbox[0]<<"\t"<<xbox[1]<<"\t"<<xbox[2]<<std::endl;
     std::cout<<"ybox"<<"\t"<<ybox[0]<<"\t"<<ybox[1]<<"\t"<<ybox[2]<<std::endl;
     std::cout<<"zbox"<<"\t"<<zbox[0]<<"\t"<<zbox[1]<<"\t"<<zbox[2]<<std::endl;
     */

  values.clear();

  //resize the array
  int sz=no_values*natoms;
  values.reserve(sz);

  std::getline(myfile,line);
  std::getline(myfile,line);


  for(int ii=0;ii<natoms;ii++){

    std::getline(myfile,line);
    std::istringstream input(line);

    while(input>>val){
      values.push_back(val);
    }

  }

  boxDim();

  return false;

}


//Function to parse the dat in
bool SimBox::readBinData(std::ifstream &myfile){

  int triclinic=1,nchunk=1,size_one=1;

  if(myfile.eof()) return true;

  //just based on LAMMPS dump format which may change slightly with different versions
  myfile.read(as_bytes(timestep),sizeBig);
  myfile.read(as_bytes(natoms),sizeBig);
  myfile.read(as_bytes(triclinic),sizeInt);
  myfile.read(as_bytes(boundary[0][0]),sizeInt*6);
  myfile.read(as_bytes(xbox[0]),sizeDouble*2);
  myfile.read(as_bytes(ybox[0]),sizeDouble*2);
  myfile.read(as_bytes(zbox[0]),sizeDouble*2);
  if (triclinic) {
    myfile.read(as_bytes(xbox[2]),sizeDouble);
    myfile.read(as_bytes(ybox[2]),sizeDouble);
    myfile.read(as_bytes(zbox[2]),sizeDouble);
  }
  myfile.read(as_bytes(size_one),sizeInt);
  myfile.read(as_bytes(nchunk),sizeInt);

  //resize the array
  int sz=no_values*natoms;
  //double* values = new double[sz];
  values.resize(sz);
  if(natoms>0)
  {
    double *pp = values.data();

    long int ndble=0;
    int step=0;

    // loop over processor chunks in file
    for (int i = 0; i<nchunk; i++) {
      myfile.read(as_bytes(ndble),sizeInt);
      myfile.read(as_bytes(pp[step]),ndble*sizeDouble);
      step+=ndble;
    }
  }

  boxDim();

  return false;

}

//calculate box dimensions
void SimBox::boxDim(){
  //Look at LAMMPS how to triclinic for more details
  boxl.resize(6);
  double xy=xbox[2];
  double xz=ybox[2];
  double yz=zbox[2];

  double xlo=xbox[0]-std::min({0.0,xy,xz,xy+xz});
  double xhi=xbox[1]-std::max({0.0,xy,xz,xy+xz});

  double ylo=ybox[0]-std::min(0.0,yz);
  double yhi=ybox[1]-std::max(0.0,yz);

  double zlo=zbox[0];
  double zhi=zbox[1];

  boxl[0]=xhi-xlo;
  boxl[1]=yhi-ylo;
  boxl[2]=zhi-zlo;

  boxl[3]=xlo;
  boxl[4]=ylo;
  boxl[5]=zlo;
}

//return value of atom i and position lpos
double SimBox::getItem(int i,int lpos) const{
  int pos=i*no_values+lpos-1;
  return values[pos];
}


//local utility functions
int periodic_char(char xx){
  int p;
  switch(xx){
    case 'p':
      p=0; break;
    case 'f':
      p=1; break;
    case 's':
      p=2; break;
    case 'm':
      p=3; break;
    default:
      std::cout<<"Read error";
      abort();
  }
  return p;
}


std::vector<std::string> split_words(std::string line)
{

  std::istringstream input(line);
  std::vector<std::string> words;
  std::copy(std::istream_iterator<std::string>(input),std::istream_iterator<std::string>(),std::back_inserter(words));

  return words;
}
