#include"box_properties.h"
#include<cmath>

bool BoxProp::readBinData(std::ifstream &myfile){

  SimBox::readBinData(myfile);
  if(shear) //shear is true
    vecBoxDisp(); //calculate box displacement based on dt,gdot

}
//read formatted data
bool BoxProp::readFormData(std::ifstream &myfile){

  SimBox::readFormData(myfile);
  if(shear) //shear is true
    vecBoxDisp(); //calculate box displacement based on dt,gdot

}


//return displacement due to shear based on gdot
void BoxProp::vecBoxDisp(){
    
      vec_delta.resize(3);

     //   xy=xbox[2], yhi-ylo=boxl[1]
      vec_delta[0]=xbox[2]-dt*vec_gdot[0]*boxl[1];

      //   xz=ybox[2], zhi-zlo=boxl[2]
      vec_delta[1]=ybox[2]-dt*vec_gdot[1]*boxl[2];

      //   yz=zbox[2], zhi-zlo=boxl[2] 
      vec_delta[2]=zbox[2]-dt*vec_gdot[2]*boxl[2];

}


