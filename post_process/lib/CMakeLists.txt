# lists of files generated from ls (other scripts in general)
file(STRINGS "include_list.txt" inc_list)
file(STRINGS "src_list.txt" src_list)

add_library(
	rw-files
	${inc_list}
	${src_list}
	)

# target, interface, directory to include
target_include_directories(rw-files PUBLIC "$(CMAKE_CURRENT_SOURCE_DIR)/include")

#target_include_directories(rw-files PUBLIC "$(CMAKE_CURRENT_SOURCE_DIR)/src")

##example comment
#target_compile_definitions(say-hello INTERFACE SAY_HELLO_VERSION=4)
