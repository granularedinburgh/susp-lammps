cmake_minimum_required(VERSION 2.8)
project(POVRW)

#SET(GCC_DEBUG_FLAGS "-std=c++11 -Wshadow -Wall -Wextra -pedantic -g")
SET(GCC_SPEED_FLAGS "-O3 -std=c++11 -march=native")

SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_SPEED_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_SPEED_FLAGS}")

#add subdirectory
add_subdirectory(lib)
#add subdirectory
add_subdirectory(programs)

#Bring the headers, such as Student.h into the project
##include_directories(include)
 
#Can manually add the sources using the set command as follows:
#set(SOURCES src/mainapp.cpp src/Student.cpp)
 
#However, the file(GLOB...) allows for wildcard additions:
#file(GLOB SOURCES "lib/*.cpp")
