//updated dump file read and output pov
//author: Ranga (20/9/2019)
#include"definitions.h"
#include"helper.h"
#include"read_input.h"
#include"pov_box.h"
#include"force_box.h"
#include<ctime>
#include<iostream>
#include<fstream>
#include<cmath>


//#include<gsl/gsl_histogram.h>


using namespace std;

int main (int argc, char **argv)
{
  std::clock_t start=std::clock();
  //declare some default values
  int no_values=19;
  double dt=1E-4;
  double gdot=0.01;
  VEC_DBLE vec_gdot{0.0,0.0,0.0};
  int type_pos=2,rad_pos=3,x_pos=5;
  STRING dumpFName="dump.bin";
  int noBig=250;


  if( read_term_cntct(argc,argv,noBig,no_values,vec_gdot,dt,dumpFName) !=0 ){
    std::cerr<<"read error of command line";
    return 1;
  }

  //read input file
  ifstream dumpfile;
  STRING ending=dumpFName.substr(dumpFName.find_last_of('.')+1);
  bool binary_file;
  (ending=="bin")?binary_file=true:binary_file=false;


  if(binary_file)
    dumpfile.open(dumpFName,ios_base::binary);
  else
    dumpfile.open(dumpFName);


  if(dumpfile.fail()){
    cerr<<"Couldn't open the file!"<<endl;
    return 1;
  }


  PoVBox dump_dat(no_values,dt,vec_gdot,type_pos,x_pos,rad_pos);

  for(int count=0;dumpfile.peek()!=EOF;count++){
    if(binary_file)
      //read dump data
      dump_dat.readBinData(dumpfile);
    else
      dump_dat.readFormData(dumpfile);


    //write box file
    dump_dat.writeBox(count);

    //get contactlist and atom list just from the position dumpfile (optional Kn value to scale the seperation distance as forces)
    ATOM_CONTACT_LIST aList=dump_dat.output_list();

    //write bonds and atoms in output file in the pov_files folder 
    dump_dat.writeAtomsBonds(count,aList);
  }

  dumpfile.close();

  std::cout<< "Run time is:"<< (std::clock()-start)/(1.0*CLOCKS_PER_SEC)<<std::endl;

  return 0;


}
