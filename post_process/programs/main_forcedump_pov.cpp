/*!
 * \author Ranga Radhakrishnan
 * \version 1.0
 * \date 06/11/2019
 *
 * \bug 
 *
 * \warning Default options for the dump file positions are type=2, radius=3, x_pos=5
 *
 * \copyright GNU Public License
 *
 * \mainpage Post-processing scripts for LAMMPS
 *
 */ 
#include"definitions.h"
#include"helper.h"
#include"read_input.h"
#include"pov_box.h"
#include"force_box.h"
#include<ctime>
#include<iostream>
#include<fstream>
#include<cmath>

#include"simbox.h"
//#include<gsl/gsl_histogram.h>


using namespace std;

int main (int argc, char **argv)
{
  std::clock_t start=std::clock();
  //declare some default values
  int no_values=19;
  double dt=1E-4;
  double gdot=0.01;
  VEC_DBLE vec_gdot{0.0,0.0,0.0};
  int type_pos=2,rad_pos=3,x_pos=5;
  STRING dumpFName="dump.bin";
  STRING forceFName="forces.lammpstrj";
  CTYPE contactType=frictionless;

  //read parameters
  if(read_term_pov(argc,argv,no_values,vec_gdot,dt,dumpFName,forceFName,contactType)!=0){
    std::cerr<<"read error of command line";
    return 1;
  }

  //read input files
  ifstream dumpfile,forcefile;
  STRING ending=dumpFName.substr(dumpFName.find_last_of('.')+1);
  bool binary_file;
  (ending=="bin")?binary_file=true:binary_file=false;


  if(binary_file)
    dumpfile.open(dumpFName,ios_base::binary);
  else
    dumpfile.open(dumpFName);

  if(dumpfile.fail()){
    cerr<<"Couldn't open the dump file!"<<endl;
    return 1;
  }

  forcefile.open(forceFName);
  if(forcefile.fail()){
    cerr<<"Couldn't open the force file!"<<endl;
    return 1;
  }

  // initialize object of PoVBox and Force_Box
  ForceBox force_dat;

  // count variable mainly for dump file output
  for(int count=0;dumpfile.peek()!=EOF && forcefile.peek()!=EOF;count++){

    //read force data
    force_dat.readForceData(forcefile);
    int force_time=force_dat.getTime();

    PoVBox dump_dat(no_values,dt,vec_gdot,type_pos,x_pos,rad_pos);
    // SimBox dump_dat(no_values);


    if(binary_file)
      //read dump data
      dump_dat.readBinData(dumpfile);
    else
      dump_dat.readFormData(dumpfile);

    int dump_time=dump_dat.getTime();

    if(force_time == dump_time){
      //read output box
      dump_dat.writeBox(count);


      //contactList and aList are populated
      ATOM_CONTACT_LIST acList=force_dat.getAtomsContacts(contactType);

      //write bonds and atoms in output file in the pov_files folder 
      dump_dat.writeAtomsBonds(count,acList);
    }
    else
      std::cout<<"Force time:"<<force_time<<"\t is not equal to dump time:"<<dump_time<<std::endl;

  }

  dumpfile.close();
  forcefile.close();

  std::cout<< "Run time is:"<< (std::clock()-start)/(1.0*CLOCKS_PER_SEC)<<std::endl;

  return 0;
}
