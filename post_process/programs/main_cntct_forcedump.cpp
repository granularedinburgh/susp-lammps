//Read data using cpp file (28/01/2019)
#include"definitions.h"
#include<iostream>
#include<fstream>
#include"force_box.h"
#include"helper.h"
#include"read_input.h"
//#include<gsl/gsl_histogram.h>



int main (int argc, char **argv)
{

  int no_atoms=1872;
  STRING force_file="forces.lammpstrj"; 

  if(read_force_cntct(argc,argv,no_atoms,force_file)!=0){
    std::cerr<<"read error of command line";
    return 1;
  }

  std::ifstream myfile;
  myfile.open(force_file);

  if(myfile.fail()){
    std::cerr<<"Couldn't open the file!";
    return 1;
  }


    //ForceBox fdat;
    //fdat.readFormData(myfile);
    //fdat.test();

  VEC_DBLE cntct_no;

 ForceBox fdat;

  while( myfile.peek() != EOF ){
    fdat.readForceData(myfile);

    int nContcts=0,nEntries=0;
    nEntries=fdat.getNentries();

    for(int ii=0;ii<nEntries;ii++)
      if (fdat.getCtype(ii)!=0) nContcts++;

    double cno=(2.0*nContcts)/(1.0*no_atoms);
    cntct_no.push_back(cno);
  }
  myfile.close();

  //calculate averages
  VEC_DBLE ave_tot=mean_dev(cntct_no);


  //output file0
  std::ofstream avecntct;
  avecntct.open("force_contct_no.dat",std::ofstream::out);
  avecntct<<ave_tot[0]<<"\t"<<ave_tot[1]<<std::endl;
  avecntct.close();




  return 0;
}
