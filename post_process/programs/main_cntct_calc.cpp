//Read data using cpp file (28/01/2019)
#include"definitions.h"
#include<iostream>
#include<fstream>
#include"read_input.h"
#include"contact_box.h"
#include"helper.h"
#include<cmath>
//#include<gsl/gsl_histogram.h>


using namespace std;

int main (int argc, char **argv)
{
  // vector<BoxProp> alldat;
  int no_values=19;
  double dt=1E-4;
  int noBig=250;
  int type_pos=2,rad_pos=3,x_pos=5;
  VEC_DBLE vec_gdot{0.0,0.0,0.0};
  STRING dumpFName="dump.bin";



  if( read_term_cntct(argc,argv,noBig,no_values,vec_gdot,dt,dumpFName) !=0 ){
    std::cerr<<"read error of command line";
    return 1;
  }

  STRING ending=dumpFName.substr(dumpFName.find_last_of('.')+1);
  bool binary_file;
  (ending=="bin")?binary_file=true:binary_file=false;


  //read input file
  ifstream dumpfile;
  if(binary_file)
    dumpfile.open(dumpFName,ios_base::binary);
  else
    dumpfile.open(dumpFName);
  if(dumpfile.fail()){
    cerr<<"Couldn't open the file!"<<endl;
    return 1;
  }

  CntctBox dat(no_values,dt,vec_gdot,type_pos,x_pos,rad_pos);
  VEC_DBLE big_arr,ss_arr,tot_arr,sep,norattle_arr;

  while(dumpfile.peek()!=EOF){
    if(binary_file)
      //read dump data
      dat.readBinData(dumpfile);
    else
      dat.readFormData(dumpfile);



    int Natoms=dat.getNatoms();

    VEC_DBLE res=dat.calc_Cntct();

    ss_arr.push_back(res[0]/((Natoms-noBig)*1.0));
    big_arr.push_back(res[1]/(noBig*1.0));
    tot_arr.push_back(res[2]/(Natoms*1.0));
    sep.push_back(res[3]);

    double result=dat.calc_Cntct_exclude();
    norattle_arr.push_back(result);

  }
  dumpfile.close();

  //calculate averages
  VEC_DBLE ave_sep=mean_dev(sep);
  VEC_DBLE ave_ss=mean_dev(ss_arr);
  VEC_DBLE ave_big=mean_dev(big_arr);
  VEC_DBLE ave_tot=mean_dev(tot_arr);

  //output file0
  ofstream avecntct;
  avecntct.open("ave_contct_no.dat",ofstream::out);
  avecntct<<vec_gdot[0]<<"\t"<<vec_gdot[1]<<"\t"<<vec_gdot[2]<<"\t";
  avecntct<<ave_ss[0]<<"\t"<<ave_ss[1]<<"\t"<<ave_big[0]<<"\t"<<ave_big[1]<<"\t"<<ave_tot[0]<<"\t"<<ave_tot[1]<<endl;
  avecntct.close();

  //output file1
  ofstream aveout;
  aveout.open("ave_histo.dat");
  aveout<<vec_gdot[0]<<"\t"<<vec_gdot[1]<<"\t"<<vec_gdot[2]<<"\t";
  aveout<<ave_sep[0]<<"\t"<<ave_sep[1]<<endl;
  aveout.close();

  //output file2
  VEC_DBLE ave_norattle=mean_dev(norattle_arr);
  ofstream avedif;
  avedif.open("ave_no_rattler.dat");
  avedif<<vec_gdot[0]<<"\t"<<vec_gdot[1]<<"\t"<<vec_gdot[2]<<"\t";
  avedif<<ave_norattle[0]<<"\t"<<ave_norattle[1]<<endl;
  avedif.close();


  return 0;
}
